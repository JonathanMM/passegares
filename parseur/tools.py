import urllib
import os
from lv95Convert import LV95Convert
from lambert93Convert import Lambert93Convert

metadonnes = 141
prochaineCouleur = -1

# --------------------
# Public Methods
# --------------------


def telechargerFichier(lien, tmpFileName):
    print('Téléchargement de : ' + lien)
    # Désactivation durant les tests
    urllib.request.urlretrieve(lien, tmpFileName)
    print('Téléchargement terminé')


def getEntetes(ligne, correspondancesCol=None, separateur=';'):
    champ = ligne.strip().split(separateur)
    return getEntetesList(champ, correspondancesCol, separateur)


def getEntetesList(champ, correspondancesCol=None, separateur=';'):
    posCol = {}
    i = 0
    for c in champ:
        nom = nettoyerTexte(c.strip())
        if correspondancesCol != None and nom in correspondancesCol.keys():
            posCol[correspondancesCol[nom]] = i
        else:
            posCol[nom] = i
        i += 1
    return posCol


def getValue(key, data, defaultValue):
    if not key in list(data.keys()):
        return defaultValue[key]
    return data[key]

def getValueOrDefault(key, data, defaultValue):
    if not key in list(data.keys()):
        return defaultValue
    return data[key]


def getCouleur():
    global prochaineCouleur
    prochaineCouleur += 1
    if prochaineCouleur >= 8:
        prochaineCouleur %= 8
    return prochaineCouleur


def getCouleurEvolution(nom, couleur):
    couleurEvolution = (couleur + len(nom)) % 8
    if couleurEvolution == couleur:
        couleurEvolution = (couleur + 3) % 8
    return couleurEvolution


def getCoordonnees(gare):
    if not 'latitude' in gare and not 'longitude' in gare:
        if not 'coordonnees' in gare:
            raise Exception('Aucune coordonnées mentionnées dans le fichier')
        coupe = gare['coordonnees'].split(',')
        gare['latitude'] = coupe[0].strip()
        gare['longitude'] = coupe[1].strip()


def getXY(gare):
    if not 'x' in gare and not 'y' in gare:
        if not 'xy' in gare:
            raise Exception('Aucune couple X,Y mentionnées dans le fichier')
        coupe = gare['xy'].split(',')
        gare['x'] = coupe[0].strip()
        gare['y'] = coupe[1].strip()


def nettoyerTexte(texte):
    if texte == None:
        return ""
    texte = texte.strip()
    if texte[0:1] == '"':
        return texte[1:][:-1]
    return texte


def convertCoordonnees(convertMethod, gare):
    if convertMethod == "LV95":
        convert = LV95Convert()
        gare['latitude'], gare['longitude'] = convert.convert(
            gare['est'], gare['nord'])
    elif convertMethod == "Lambert93":
        convert = Lambert93Convert()
        getXY(gare)
        gare['latitude'], gare['longitude'] = convert.convert(
            gare['x'], gare['y'])
    else:
        raise Exception("Le convertisseur de données n'existe pas")
