package fr.nocle.passegares.visa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import fr.nocle.passegares.R;
import fr.nocle.passegares.modele.LigneTamponnee;
import fr.nocle.passegares.modele.Region;

/**
 * Created by jonathanmm on 01/10/16.
 */

public class LigneTamponneeAdapter extends BaseAdapter {
    private static final int TYPE_REGION = 0;
    private static final int TYPE_VILLE = 1;
    private static final int TYPE_LIGNE = 2;
    private Context context;
    private boolean voirTamponDuJour;
    private ArrayList<String> listeRegions;
    private ArrayList<String> listeVilles;
    private ArrayList<Integer> positionRegions;
    private ArrayList<Integer> positionVilles;
    private ArrayList<LigneTamponnee> listeLigne;

    public LigneTamponneeAdapter(Context context, ArrayList<LigneTamponnee> liste, boolean voirTamponDuJour)
    {
        this.context = context;
        this.voirTamponDuJour = voirTamponDuJour;
        this.listeLigne = liste;
        initRegion(liste);
    }

    private void initRegion(ArrayList<LigneTamponnee> listeLignes) {
        listeRegions = new ArrayList<>();
        positionRegions = new ArrayList<>();
        listeVilles = new ArrayList<>();
        positionVilles = new ArrayList<>();
        long lastRegion = -1;
        String lastVille = null;
        int currentPosition = 0;
        for(LigneTamponnee l : listeLignes) {
            if (l.getRegion().getId() != lastRegion) {
                listeRegions.add(l.getRegion().getNom());
                positionRegions.add(currentPosition);
                currentPosition++;
                lastVille = null;
            }

            String ville = l.getVille();
            if(ville != null && ville.isEmpty())
            {
                // On normalise la donnée
                ville = null;
            }
            if (!Objects.equals(ville, lastVille))
            {
                if(ville == null) {
                    listeVilles.add(this.context.getString(R.string.region));
                }
                else{
                    listeVilles.add(ville);
                }
                positionVilles.add(currentPosition);
                currentPosition++;
            }
            lastRegion = l.getRegion().getId();
            lastVille = ville;
            currentPosition++;
        }
    }

    @Override
    public int getCount() {
        return listeLigne.size() + listeRegions.size() + listeVilles.size();
    }

    @Override
    public LigneTamponnee getItem(int position) {
        return listeLigne.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        int itemType = getItemViewType(position);
        if(itemType == TYPE_LIGNE) {
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(this.context).inflate(R.layout.ligne_tamponnee_viewer, parent, false);
            }

            //Il faut trouver le nombre de région qui existent avant
            int realPosition = getRealPositionOfLigne(position);
            LigneTamponnee l = getItem(realPosition);

            TextView champ;
            champ = (TextView) convertView.findViewById(R.id.nomLigne);
            setNomLigne(champ, l.getNomLigne(), l.getRegion());

            champ = (TextView) convertView.findViewById(R.id.nbTampons);

            ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
            int nbTampons = l.getNbTampons();
            int maxTampons = l.getNbGares();
            if (voirTamponDuJour) {
                String tampon;
                if (l.getNbTampons() > 1)
                    tampon = convertView.getContext().getString(R.string.tampons);
                else
                    tampon = convertView.getContext().getString(R.string.tampon);

                champ.setText(nbTampons + " " + tampon);
                progressBar.setVisibility(View.GONE);
            } else {
                champ.setText(nbTampons + "/" + maxTampons);

                Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
                if(nbTampons == maxTampons)
                {
                    progressDrawable.setColorFilter(
                            context.getResources().getColor(
                                    R.color.succes
                            ),
                            android.graphics.PorterDuff.Mode.SRC_IN);
                } else {
                    progressDrawable.clearColorFilter();
                }
                progressBar.setProgressDrawable(progressDrawable);

                progressBar.setMax(maxTampons);
                progressBar.setProgress(nbTampons);

                progressBar.setVisibility(View.VISIBLE);
            }

            ImageView icon = (ImageView) convertView.findViewById(R.id.iconeLigne);

            setIcon(icon, l.getRegion().getId(), l.getVille(), l.getNomLigne());
        } else if(itemType == TYPE_REGION)
        {
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(this.context).inflate(R.layout.titre_nom_region_viewer, parent, false);
            }

            TextView champ = (TextView) convertView.findViewById(R.id.titreRegion);
            champ.setText(listeRegions.get(getSectionForPositionRegion(position)));
        } else if(itemType == TYPE_VILLE)
        {
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(this.context).inflate(R.layout.titre_nom_ville_viewer, parent, false);
            }

            TextView champ = (TextView) convertView.findViewById(R.id.titreVille);
            champ.setText(listeVilles.get(getSectionForPositionVille(position)));
        }
        return convertView;
    }

    public int getRealPositionOfLigne(int position) {
        int nbItems = 0;
        for(int positionItem : positionRegions)
        {
            if(positionItem < position)
                nbItems++;
        }
        for(int positionItem : positionVilles)
        {
            if(positionItem < position)
                nbItems++;
        }
        return position - nbItems;
    }

    static public void setNomLigne(TextView champ, String nomLigne, Region region)
    {
        if(nomLigne.equals("Ligne Unique") && region != null)
            champ.setText(nomLigne + " (" + region.getNom() + ")");
        else
            champ.setText(nomLigne);
    }

    private int getSectionForPositionRegion(int position) {
        return positionRegions.indexOf(position);
    }

    private int getSectionForPositionVille(int position) {
        return positionVilles.indexOf(position);
    }

    @Override
    public int getItemViewType(int position) {
        if(positionRegions.contains(position)) return TYPE_REGION;
        if(positionVilles.contains(position)) return TYPE_VILLE;
        return TYPE_LIGNE;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public boolean isEnabled(int position) {
        if (getItemViewType(position) == TYPE_REGION || getItemViewType(position) == TYPE_VILLE){
            return false;
        }
        return true;
    }

    static public void setIcon(ImageView icon, long idRegion, String ville, String nomLigne)
    {
        // Normandie
        if(idRegion == 4)
        {
            switch(nomLigne)
            {
                case "Nomad Train":
                    icon.setImageResource(R.drawable.il_normandie_ter);
                    icon.setVisibility(View.VISIBLE);
                    break;
                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        //Transports de Lille (Hauts-de-France)
        if(idRegion == 3)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "TER Hauts-de-France":
                        icon.setImageResource(R.drawable.il_hautsdefrance_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    case "Grandes Lignes":
                        icon.setImageResource(R.drawable.il_gl);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Lille": {
                    switch (nomLigne) {
                        case "Métro 1":
                            icon.setImageResource(R.drawable.il_lille_m1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Métro 2":
                            icon.setImageResource(R.drawable.il_lille_m2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram R":
                            icon.setImageResource(R.drawable.il_lille_tr);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram T":
                            icon.setImageResource(R.drawable.il_lille_tt);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Pays de la Loire
        if(idRegion == 5)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "Aléop en TER":
                        icon.setImageResource(R.drawable.il_paysdelaloire_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Nantes": {
                    switch (nomLigne)
                    {
                        case "Tramway 1":
                            icon.setImageResource(R.drawable.il_nantes_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tramway 2":
                            icon.setImageResource(R.drawable.il_nantes_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tramway 3":
                            icon.setImageResource(R.drawable.il_nantes_t3);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                    return;
                }

                case "Le Mans": {
                    switch(nomLigne)
                    {
                        case "T1":
                            icon.setImageResource(R.drawable.il_lemans_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T2":
                            icon.setImageResource(R.drawable.il_lemans_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                    return;
                }

                case "Angers": {
                    switch (nomLigne)
                    {
                        case "Tram A":
                            icon.setImageResource(R.drawable.il_angers_trama);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram B":
                            icon.setImageResource(R.drawable.il_angers_tramb);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram C":
                            icon.setImageResource(R.drawable.il_angers_tramc);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                    return;
                }

                default: {
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
            }
            return;
        }

        // Occitanie
        if(idRegion == 2)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "lio Train":
                        icon.setImageResource(R.drawable.il_occitanie_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Toulouse":
                {
                    switch(nomLigne)
                    {
                        case "Métro A":
                            icon.setImageResource(R.drawable.il_toulouse_metroa);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Métro B":
                            icon.setImageResource(R.drawable.il_toulouse_metrob);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T1":
                            icon.setImageResource(R.drawable.il_toulouse_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "téléo":
                            icon.setImageResource(R.drawable.il_toulouse_teleo);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                case "Montpellier":
                {
                    switch(nomLigne)
                    {
                        case "T1":
                            icon.setImageResource(R.drawable.il_montpellier_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T2":
                            icon.setImageResource(R.drawable.il_montpellier_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T3":
                            icon.setImageResource(R.drawable.il_montpellier_t3);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T4":
                            icon.setImageResource(R.drawable.il_montpellier_t4);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Nouvelle-Aquitaine
        if(idRegion == 8)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "TER Nouvelle Aquitaine":
                        icon.setImageResource(R.drawable.il_nouvelleaquitaine_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Bordeaux":
                {
                    switch(nomLigne)
                    {
                        case "Tram A":
                            icon.setImageResource(R.drawable.il_bordeaux_trama);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram B":
                            icon.setImageResource(R.drawable.il_bordeaux_tramb);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram C":
                            icon.setImageResource(R.drawable.il_bordeaux_tramc);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tram D":
                            icon.setImageResource(R.drawable.il_bordeaux_tramd);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Auvergne - Rhône Alpes
        if(idRegion == 11)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "TER Auvergne Rhône Alpes":
                        icon.setImageResource(R.drawable.il_aura_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Grenoble":
                {
                    switch(nomLigne)
                    {
                        case "Ligne A":
                            icon.setImageResource(R.drawable.il_grenoble_trama);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Ligne B":
                            icon.setImageResource(R.drawable.il_grenoble_tramb);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Ligne C":
                            icon.setImageResource(R.drawable.il_grenoble_tramc);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Ligne D":
                            icon.setImageResource(R.drawable.il_grenoble_tramd);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Ligne E":
                            icon.setImageResource(R.drawable.il_grenoble_trame);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Bretagne
        if(idRegion == 12)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "TER BreizhGo":
                        icon.setImageResource(R.drawable.il_bretagne_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }
            switch (ville)
            {
                case "Rennes":
                {
                    switch(nomLigne)
                    {
                        case "Métro A":
                            icon.setImageResource(R.drawable.il_rennes_metro_a);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Métro B":
                            icon.setImageResource(R.drawable.il_rennes_metro_b);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                case "Brest":
                {
                    switch(nomLigne)
                    {
                        case "Ligne A":
                            icon.setImageResource(R.drawable.il_brest_tram_a);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Ligne C":
                            icon.setImageResource(R.drawable.il_brest_telepherique_c);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Grand Est
        if(idRegion == 9)
        {
            switch(nomLigne)
            {
                case "fluo":
                    icon.setImageResource(R.drawable.il_grandest_ter);
                    icon.setVisibility(View.VISIBLE);
                    break;
                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        //Rémi Centre Val de Loire
        if(idRegion == 10)
        {
            switch(nomLigne)
            {
                case "Rémi Centre-Val de Loire":
                    icon.setImageResource(R.drawable.il_centrevdl_ter);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Grandes Lignes":
                    icon.setImageResource(R.drawable.il_gl);
                    icon.setVisibility(View.VISIBLE);
                    break;
                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Provence Alpes Côte d'Azur
        if(idRegion == 6)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "TER Zou !":
                        icon.setImageResource(R.drawable.il_paca_terzou);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    case "Chemins de Fer de Provence":
                        icon.setImageResource(R.drawable.il_paca_cheminsferprovence);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch(ville)
            {
                case "Nice": {
                    switch(nomLigne)
                    {
                        case "Tramway 1":
                            icon.setImageResource(R.drawable.il_nice_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tramway 2":
                            icon.setImageResource(R.drawable.il_nice_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "Tramway 3":
                            icon.setImageResource(R.drawable.il_nice_t3);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                    return;
                }

                case "Avignon": {
                    switch(nomLigne)
                    {
                        case "T1":
                            icon.setImageResource(R.drawable.il_avignon_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                    return;
                }

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Bourgogne Franche-Comté
        if(idRegion == 7)
        {
            if(ville == null || ville.isEmpty())
            {
                switch(nomLigne)
                {
                    case "Mobigo":
                        icon.setImageResource(R.drawable.il_bourgognefc_ter);
                        icon.setVisibility(View.VISIBLE);
                        break;
                    default:
                        icon.setImageDrawable(null);
                        icon.setVisibility(View.INVISIBLE);
                        break;
                }
                return;
            }

            switch (ville)
            {
                case "Dijon": {
                    switch (nomLigne) {
                        case "T1":
                            icon.setImageResource(R.drawable.il_dijon_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T2":
                            icon.setImageResource(R.drawable.il_dijon_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                case "Besançon": {
                    switch (nomLigne) {
                        case "T1":
                            icon.setImageResource(R.drawable.il_besancon_t1);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        case "T2":
                            icon.setImageResource(R.drawable.il_besancon_t2);
                            icon.setVisibility(View.VISIBLE);
                            break;
                        default:
                            icon.setImageDrawable(null);
                            icon.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
                break;

                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Île-de-France
        if(idRegion == 1)
        {
            switch (nomLigne)
            {
                case "RER A":
                    icon.setImageResource(R.drawable.il_paris_rer_a);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER B":
                    icon.setImageResource(R.drawable.il_paris_rer_b);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER C":
                    icon.setImageResource(R.drawable.il_paris_rer_c);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER D":
                    icon.setImageResource(R.drawable.il_paris_rer_d);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER E":
                    icon.setImageResource(R.drawable.il_paris_rer_e);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien H":
                    icon.setImageResource(R.drawable.il_paris_transilien_h);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien J":
                    icon.setImageResource(R.drawable.il_paris_transilien_j);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien K":
                    icon.setImageResource(R.drawable.il_paris_transilien_k);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien L":
                    icon.setImageResource(R.drawable.il_paris_transilien_l);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien N":
                    icon.setImageResource(R.drawable.il_paris_transilien_n);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien P":
                    icon.setImageResource(R.drawable.il_paris_transilien_p);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien R":
                    icon.setImageResource(R.drawable.il_paris_transilien_r);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien U":
                    icon.setImageResource(R.drawable.il_paris_transilien_u);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Transilien V":
                    icon.setImageResource(R.drawable.il_paris_transilien_v);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 1":
                    icon.setImageResource(R.drawable.il_paris_metro_m1);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 2":
                    icon.setImageResource(R.drawable.il_paris_metro_m2);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 3":
                    icon.setImageResource(R.drawable.il_paris_metro_m3);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 3bis":
                    icon.setImageResource(R.drawable.il_paris_metro_m3bis);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 4":
                    icon.setImageResource(R.drawable.il_paris_metro_m4);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 5":
                    icon.setImageResource(R.drawable.il_paris_metro_m5);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 6":
                    icon.setImageResource(R.drawable.il_paris_metro_m6);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 7":
                    icon.setImageResource(R.drawable.il_paris_metro_m7);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 7bis":
                    icon.setImageResource(R.drawable.il_paris_metro_m7bis);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 8":
                    icon.setImageResource(R.drawable.il_paris_metro_m8);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 9":
                    icon.setImageResource(R.drawable.il_paris_metro_m9);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 10":
                    icon.setImageResource(R.drawable.il_paris_metro_m10);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 11":
                    icon.setImageResource(R.drawable.il_paris_metro_m11);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 12":
                    icon.setImageResource(R.drawable.il_paris_metro_m12);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 13":
                    icon.setImageResource(R.drawable.il_paris_metro_m13);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Métro 14":
                    icon.setImageResource(R.drawable.il_paris_metro_m14);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T1":
                    icon.setImageResource(R.drawable.il_paris_tram_t1);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T2":
                    icon.setImageResource(R.drawable.il_paris_tram_t2);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T3a":
                    icon.setImageResource(R.drawable.il_paris_tram_t3a);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T3b":
                    icon.setImageResource(R.drawable.il_paris_tram_t3b);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T4":
                    icon.setImageResource(R.drawable.il_paris_tram_t4);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T5":
                    icon.setImageResource(R.drawable.il_paris_tram_t5);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T6":
                    icon.setImageResource(R.drawable.il_paris_tram_t6);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T7":
                    icon.setImageResource(R.drawable.il_paris_tram_t7);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T8":
                    icon.setImageResource(R.drawable.il_paris_tram_t8);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T9":
                    icon.setImageResource(R.drawable.il_paris_tram_t9);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T10":
                    icon.setImageResource(R.drawable.il_paris_tram_t10);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T11":
                    icon.setImageResource(R.drawable.il_paris_tram_t11);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T12":
                    icon.setImageResource(R.drawable.il_paris_tram_t12);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Tramway T13":
                    icon.setImageResource(R.drawable.il_paris_tram_t13);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Grandes Lignes":
                    icon.setImageResource(R.drawable.il_gl);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "orlyval":
                    icon.setImageResource(R.drawable.il_iledefrance_orlyval);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "CDGVAL":
                    icon.setImageResource(R.drawable.il_iledefrance_cdgval);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "Funiculaire de Montmartre":
                    icon.setImageResource(R.drawable.il_iledefrance_fun);
                    icon.setVisibility(View.VISIBLE);
                    break;
                default:
                    icon.setImageDrawable(null);
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }
            return;
        }

        // Absolument rien trouvé
        icon.setImageDrawable(null);
        icon.setVisibility(View.INVISIBLE);
    }
}
