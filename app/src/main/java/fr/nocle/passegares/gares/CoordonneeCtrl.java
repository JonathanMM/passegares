package fr.nocle.passegares.gares;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

import java.util.ArrayList;

import fr.nocle.passegares.bdd.GareBDD;
import fr.nocle.passegares.controlleur.Controlleur;
import fr.nocle.passegares.modele.Gare;

/**
 * Created by user on 25/08/2017.
 */

public class CoordonneeCtrl extends Controlleur {
    private final Context contexte;

    public CoordonneeCtrl(Context contexte) {
        super(contexte);
        this.contexte = contexte;
        this.open();
    }

    public CoordonneeCtrl(SQLiteDatabase bdd) {
        super(bdd);
        this.contexte = null;
    }

    private static ContentValues creerObjet(long idGare, double latitude, double longitude)
    {
        ContentValues valeur = new ContentValues();
        valeur.put(CoordonneeBDD.TABLE_ID_GARE, idGare);
        valeur.put(CoordonneeBDD.TABLE_LATITUDE, latitude);
        valeur.put(CoordonneeBDD.TABLE_LONGITUDE, longitude);
        return valeur;
    }

    public void create(Gare g)
    {
        this.create(g, g.getLatitude(), g.getLongitude());
    }

    public void create(Gare g, double latitude, double longitude)
    {
        ContentValues valeurs = creerObjet(g.getId(), latitude, longitude);
        bdd.insert(CoordonneeBDD.TABLE_NOM, null, valeurs);
    }

    public void delete(Gare g)
    {
        bdd.delete(CoordonneeBDD.TABLE_NOM, CoordonneeBDD.TABLE_ID_GARE + " = ?", new String[] {String.valueOf(g.getId())});
    }

    public ArrayList<Coordonnee> getNearlest(Location position)
    {
        Cursor c = bdd.query(CoordonneeBDD.TABLE_NOM, new String[] {
                        CoordonneeBDD.TABLE_ID_GARE,
                        CoordonneeBDD.TABLE_LATITUDE,
                        CoordonneeBDD.TABLE_LONGITUDE,
                },
                CoordonneeBDD.TABLE_LATITUDE + " >= ? AND " +
                        CoordonneeBDD.TABLE_LATITUDE + " <= ? AND " +
                        CoordonneeBDD.TABLE_LONGITUDE + " >= ? AND " +
                        CoordonneeBDD.TABLE_LONGITUDE + " <= ?",
                new String[] {
                        String.valueOf(position.getLatitude() - 0.01),
                        String.valueOf(position.getLatitude() + 0.01),
                        String.valueOf(position.getLongitude() - 0.015),
                        String.valueOf(position.getLongitude() + 0.015)
                },
                null, null, null);
        ArrayList<Coordonnee> listeCoordonnee = new ArrayList<>();
        while (c.moveToNext()) {
            Coordonnee g = new Coordonnee(
                    c.getLong(0),
                    c.getDouble(1),
                    c.getDouble(2));
            listeCoordonnee.add(g);
        }
        c.close();
        return listeCoordonnee;
    }
}
