#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml
import os
import csv
import tools
from csvParser import csvParser
from gtfsParser import gtfsParser
from kmlParser import kmlParser
from jsonParser import jsonParser

metadonnees = 176
seulementUnFichier = "idfm_csv.yml"
tools.metadonnes = metadonnees

# On récupère le fichier de config
scriptFolder = os.path.dirname(os.path.realpath(__file__)) + "/"
for filePath in os.listdir(scriptFolder + 'config/'):
    if seulementUnFichier != None:
        if filePath != seulementUnFichier:
            continue
    fConfig = open(scriptFolder+"config/"+filePath, 'r+')
    config = yaml.load(fConfig, Loader=yaml.FullLoader)
    fConfig.close()

    # Étape 0 : On regarde le type de fichier
    typeConfig = config['typeFile']
    if typeConfig == 'csv':
        parser = csvParser(config, metadonnees)
        parser.parse()
    elif typeConfig == 'gtfs':
        parser = gtfsParser(config, metadonnees)
        parser.parse()
    elif typeConfig == 'kml':
        parser = kmlParser(config, metadonnees)
        parser.parse()
    elif typeConfig == 'json':
        parser = jsonParser(config, metadonnees)
        parser.parse()

print('Fin de l\'import')
