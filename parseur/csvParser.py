import tools
import os
import csv
from regionManager import RegionManager
from regionManagerFactory import RegionManagerFactory


class csvParser:
    def __init__(self, config, metadonnees):
        self.config = config
        self.metadonnees = metadonnees
        self.tmpFileName = "data.csv"

    def parse(self):
        # Étape 0 : Détermination du séparateur
        separator = ';'
        if 'separator' in list(self.config.keys()):
            separator = self.config["separator"]
        print("Séparateur utilisé : " + separator)
        dialect = csv.unix_dialect
        dialect.delimiter = separator

        # Étape 0bis : On regarde ce qu’on va importer
        importerLigne = True
        if 'importType' in list(self.config.keys()):
            importerLigne = tools.getValueOrDefault('lignes', self.config['importType'], True)

        # Étape 1 : Téléchargement du fichier
        if not 'localFile' in list(self.config.keys()) or not self.config['localFile']:
            lien = self.config['link_download']
            tools.telechargerFichier(lien, self.tmpFileName)

        encodingFile = None
        if 'encodingFile' in list(self.config.keys()):
            if self.config['encodingFile'] == "UTF8BOM":
                encodingFile = 'utf-8-sig'
            else:
                encodingFile = self.config['encodingFile']

        with open(self.tmpFileName, 'r+', encoding=encodingFile) as fichierData:
            fData = csv.reader(fichierData, dialect)

            # Étape 2 : On récupère ses entêtes
            titre = next(fData)
            posCol = tools.getEntetesList(
                titre, self.config['header'], separator)
            print('Liste des entêtes : ')
            print(posCol)

            # Étape pré-3 : On prépare un tableau de Region Manager
            regionManagerFactory = RegionManagerFactory(self.config, self.metadonnees)

            regionsVues = {}

            # Étape 3 : On lit le fichier
            for contenu in fData:
                gare = {}
                for c, i in posCol.items():
                    gare[c] = contenu[i]

                if 'convertCoordonateOrigin' in list(self.config.keys()):
                    tools.convertCoordonnees(self.config['convertCoordonateOrigin'], gare)

                if 'ignoreGaresWithoutLocation' in list(self.config.keys()) and self.config['ignoreGaresWithoutLocation'] and (len(gare['latitude']) == 0 or len(gare['longitude']) == 0):
                    continue

                if 'filter' in list(self.config.keys()):
                    isValid = True
                    for key in self.config['filter']:
                        if not key in gare or gare[key] != self.config['filter'][key]:
                            isValid = False
                            break
                    if not isValid:
                        continue

                # On va regarder la région
                regionGare = tools.getValue(
                    'dossierId', gare, self.config['default_value'])
                gare['region'] = regionGare
                # A-t-elle une valeur normalisée ?
                if self.config['substitue'] != None and 'dossierId' in self.config['substitue'] and regionGare in self.config['substitue']['dossierId'].keys():
                    dossierId = self.config['substitue']['dossierId'][regionGare]
                else:
                    dossierId = regionGare
                gare['dossierId'] = dossierId

                if not dossierId in regionsVues.keys():
                    regionsVues[dossierId] = regionGare

                if len(regionGare) > 0 and (self.config['ignore_values'] == None or not 'dossierId' in self.config['ignore_values'] or not regionGare in self.config['ignore_values']['dossierId']):
                    if self.config['substitue'] != None and 'idExterne' in self.config['substitue'] and gare['idExterne'] in self.config['substitue']['idExterne']:
                        gare['idExterne'] = str(
                            self.config['substitue']['idExterne'][gare['idExterne']])
                    if self.config['default_value'] != None and 'idExterne' in self.config['default_value'] and self.config['default_value']['idExterne'] == "auto":
                        gare['idExterne'] = '__auto__'
                    # On récupère le fichier de la région
                    regionMgt = regionManagerFactory.getRegionManager(
                        gare['dossierId'], gare['region'])
                    fRegionInfos = regionMgt.getFichierRegion()

                    if self.config['ignore_values'] != None and 'idExterne' in self.config['ignore_values'] and gare['idExterne'] in self.config['ignore_values']['idExterne']:
                        print("Gare explicitement ignorée")
                        continue

                    if not gare['idExterne'] in fRegionInfos['idGares']:
                        # Nouvelle gare
                        regionMgt.addGare(gare)
                    else:
                        # On met à jour la gare
                        regionMgt.updateGare(gare)
                else:
                    print("Région non trouvée ou valeur ignorée")

            # Étape 4 : Post traitement
            for dossierId, regionGare in regionsVues.items():
                regionMgt = regionManagerFactory.getRegionManager(dossierId, regionGare)
                regionMgt.save()

        # Étape 5 : On efface le fichier data
        if not 'localFile' in list(self.config.keys()) or not self.config['localFile']:
            os.remove(self.tmpFileName)
