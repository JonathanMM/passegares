PasseGares est une nouvelle forme pour découvrir de nouveaux horizons ferroviaires de façon ludique. Voyager de gare en gare afin de remplir votre visa de tampons. Le but ultime ? Visiter toutes les gares du monde !

Contient actuellement de nombreux réseaux urbains ainsi que les gares de France, de Belgique, de Suisse, de Luxembourg, d'Espagne, du Portugal et d'Irlande. Vous voulez avoir l'application pour votre agglomération ? Envoyez nous votre demande par e-mail et elle fera peut-être partie des mises à jour ;)

L'application est amie de votre vie privée, aucune information sur les gares tamponnées ne sort de votre appareil :)

Le logo de l'application a été fait avec des icônes de fontawesome.io
