RailTrip is a new way to discover new stations in a fun way. Travel from station to station to fill your visa with stamps. The ultimate goal? Visit all the stations around the world!

Currently contains many urban networks as well as stations in France, Belgium, Switzerland, Luxembourg, Spain, Portugal and Ireland. Do you want to have this application for your city? Send us your request by e-mail and it may be part of an updates ;)

The application is a friend of your privacy, no information on the stamped stations leaves your device :)

The application logo was made with icons from fontawesome.io