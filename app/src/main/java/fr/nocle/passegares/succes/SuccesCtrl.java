package fr.nocle.passegares.succes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import fr.nocle.passegares.R;
import fr.nocle.passegares.controlleur.Controlleur;
import fr.nocle.passegares.controlleur.LigneCtrl;
import fr.nocle.passegares.modele.Ligne;

/**
 * Created by user on 25/08/2017.
 */

public class SuccesCtrl extends Controlleur {
    private final Context contexte;

    public SuccesCtrl(Context contexte) {
        super(contexte);
        this.contexte = contexte;
        this.open();
    }

    public SuccesCtrl(SQLiteDatabase bdd) {
        super(bdd);
        this.contexte = null;
    }

    public int quantiteProchainPalier(int type)
    {
        Cursor c = bdd.query(SuccesBDD.TABLE_NOM,
                new String[] {SuccesBDD.TABLE_CLE, SuccesBDD.TABLE_QTE_NECESSAIRE},
                SuccesBDD.TABLE_TYPE + " = ? AND " + SuccesBDD.TABLE_EST_VALIDE + " = ?",
                new String[] {String.valueOf(type), String.valueOf(SuccesManager.EstNonValide)},
                        null,
                        null,
                SuccesBDD.TABLE_NIVEAU + " ASC");
        if(!c.moveToFirst())
        {
            c.close();
            return -1;
        }
        int quantite = c.getInt(1);
        c.close();
        return quantite;
    }

    public void validerNiveauSuivant(int type)
    {
        Cursor c = bdd.query(SuccesBDD.TABLE_NOM,
                new String[] {SuccesBDD.TABLE_CLE},
                SuccesBDD.TABLE_TYPE + " = ? AND " + SuccesBDD.TABLE_EST_VALIDE + " = ?",
                new String[] {String.valueOf(type), String.valueOf(SuccesManager.EstNonValide)},
                null,
                null,
                SuccesBDD.TABLE_NIVEAU + " ASC");
        if(!c.moveToFirst())
        {
            c.close();
            return;
        }
        long id = c.getLong(0);
        c.close();

        this.validerSucces(id);
    }

    public void validerSucces(Succes succes)
    {
        this.validerSucces(succes.getId());
    }

    public void validerSucces(long id)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SuccesBDD.TABLE_EST_VALIDE, SuccesManager.EstValide);
        bdd.update(SuccesBDD.TABLE_NOM, contentValues, SuccesBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(id)});
    }

    public ArrayList<Succes> getAllSucces()
    {
        return this.getSucces("", new String[] {});
    }

    private ArrayList<Succes> getSucces(String where, String[] whereParameters)
    {
        Cursor c = bdd.query(SuccesBDD.TABLE_NOM,
                new String[] {
                        SuccesBDD.TABLE_CLE,
                        SuccesBDD.TABLE_TYPE,
                        SuccesBDD.TABLE_NIVEAU,
                        SuccesBDD.TABLE_QTE_NECESSAIRE,
                        SuccesBDD.TABLE_EST_VALIDE,
                        SuccesBDD.TABLE_NOM_SUCCES,
                        SuccesBDD.TABLE_CONDITION
                },
                where,
                whereParameters,
                null,
                null,
                SuccesBDD.TABLE_TYPE + " ASC, " + SuccesBDD.TABLE_NIVEAU + " ASC");
        if(!c.moveToFirst())
        {
            c.close();
            return new ArrayList<Succes>();
        }

        ArrayList<Succes> listeSucces = new ArrayList<>();
        int lastType = -1;
        boolean lastEstValide = true;
        do
        {
            long id = c.getLong(0);
            int type = c.getInt(1);
            int niveau = c.getInt(2);

            boolean estValide = c.getInt(4) == SuccesManager.EstValide;

            //Et s'il faut l'afficher ou non
            boolean estAffiche;
            String label;
            if(type == SuccesManager.TypeGare || type == SuccesManager.TypeLigne || type == SuccesManager.TypeValidation)
            {
                //On doit déterminer le label du succès
                int idLabel = getLabel(type, niveau);
                if(contexte != null) {
                    label = contexte.getString(idLabel);
                } else {
                    // On ne devrait pas arriver ici, ce code là ne sert que pour de l’affichage
                    label = "";
                }

                if(type != lastType || estValide) //On change de type, on affiche toujours le premier succès, ou si validé, alors on l'affiche toujours
                    estAffiche = true;
                else //Même type mais pas validé
                {
                    //Si c'est le premier a être non validé, alors on l'affiche quand même
                    estAffiche = lastEstValide;
                }
            } else {
                estAffiche = true;
                label = c.getString(5);
            }

            Succes s = new Succes(id, type, label, estValide, estAffiche, c.getInt(3), c.getString(6));
            listeSucces.add(s);

            lastType = type;
            lastEstValide = estValide;
        } while(c.moveToNext());
        c.close();
        return listeSucces;
    }

    public int getLabelProchainNiveau(int type)
    {
        Cursor c = bdd.query(SuccesBDD.TABLE_NOM,
                new String[] {SuccesBDD.TABLE_NIVEAU},
                SuccesBDD.TABLE_TYPE + " = ? AND " + SuccesBDD.TABLE_EST_VALIDE + " = ?",
                new String[] {String.valueOf(type), String.valueOf(SuccesManager.EstNonValide)},
                null,
                null,
                SuccesBDD.TABLE_NIVEAU + " ASC");
        if(!c.moveToFirst())
        {
            c.close();
            return 0;
        }
        int niveau = c.getInt(0);
        c.close();

        return getLabel(type, niveau);
    }

    private int getLabel(int type, int niveau) {
        switch(type)
        {
            case SuccesManager.TypeGare:
                switch(niveau)
                {
                    case 1:
                        return R.string.succesLabelGareNiveau1;
                    case 2:
                        return R.string.succesLabelGareNiveau2;
                    case 3:
                        return R.string.succesLabelGareNiveau3;
                    case 4:
                        return R.string.succesLabelGareNiveau4;
                    case 5:
                        return R.string.succesLabelGareNiveau5;
                    case 6:
                        return R.string.succesLabelGareNiveau6;
                    case 7:
                        return R.string.succesLabelGareNiveau7;
                    case 8:
                        return R.string.succesLabelGareNiveau8;
                }
            case SuccesManager.TypeLigne:
                switch(niveau) {
                    case 1:
                        return R.string.succesLabelLigneNiveau1;
                    case 2:
                        return R.string.succesLabelLigneNiveau2;
                    case 3:
                        return R.string.succesLabelLigneNiveau3;
                    case 4:
                        return R.string.succesLabelLigneNiveau4;
                    case 5:
                        return R.string.succesLabelLigneNiveau5;
                    case 6:
                        return R.string.succesLabelLigneNiveau6;
                    case 7:
                        return R.string.succesLabelLigneNiveau7;
                }
            case SuccesManager.TypeValidation:
                switch(niveau) {
                    case 1:
                        return R.string.succesLabelValidationNiveau1;
                    case 2:
                        return R.string.succesLabelValidationNiveau2;
                    case 3:
                        return R.string.succesLabelValidationNiveau3;
                    case 4:
                        return R.string.succesLabelValidationNiveau4;
                    case 5:
                        return R.string.succesLabelValidationNiveau5;
                }
        }

        return 0; //On ne devrait pas arriver ici
    }

    public static ContentValues creerRegionalSucces(String idExterne, String nom, int type, String condition, long idRegion)
    {
        ContentValues valeur = new ContentValues();
        valeur.put(SuccesBDD.TABLE_ID_EXTERNE, idExterne);
        valeur.put(SuccesBDD.TABLE_ID_REGION, idRegion);
        valeur.put(SuccesBDD.TABLE_NOM_SUCCES, nom);
        valeur.put(SuccesBDD.TABLE_TYPE, type);
        valeur.put(SuccesBDD.TABLE_CONDITION, condition);
        return valeur;
    }

    public void create(String idExterne, String nom, int type, String condition, long idRegion)
    {
        ContentValues valeurs = creerRegionalSucces(idExterne, nom, type, condition, idRegion);
        valeurs.put(SuccesBDD.TABLE_EST_VALIDE, 0);
        bdd.insert(SuccesBDD.TABLE_NOM, null, valeurs);
    }

    public void update(String idExterne, String nom, int type, String condition, long idRegion)
    {
        bdd.update(SuccesBDD.TABLE_NOM, creerRegionalSucces(idExterne, nom, type, condition, idRegion), SuccesBDD.TABLE_ID_EXTERNE + " = ?", new String[] {String.valueOf(idExterne)});
    }

    public void delete(String idExterne)
    {
        bdd.delete(SuccesBDD.TABLE_NOM, SuccesBDD.TABLE_ID_EXTERNE + " = ?", new String[] {String.valueOf(idExterne)});
    }

    public void setProgression(Succes succes)
    {
        if(this.contexte == null)
        {
            return;
        }

        if(succes.getType() != SuccesManager.TypeLigneRegion)
        {
            // Seul type qu’on gère pour le moment
            return;
        }

        LigneCtrl ligneControlleur = new LigneCtrl(this.contexte);

        // On doit comprendre les conditions, qui sont un ensemble d’id de ligne
        String[] idLignes = succes.getConditions().split(",");

        int nbLignesTotal = idLignes.length;
        int nbLignesEnCours = 0;
        for (String idLigneExterne: idLignes) {
            Ligne ligne = ligneControlleur.get(idLigneExterne);
            if(ligne == null)
            {
                Log.e("Succes", "Ligne " + idLigneExterne + " non trouvée");
                succes.setProgression(0);
                succes.setObjectif(0);
                return;
            }
            if(ligneControlleur.estTotalementTamponnee(ligne.getId()))
            {
                nbLignesEnCours++;
            }
        }
        succes.setProgression(nbLignesEnCours);
        succes.setObjectif(nbLignesTotal); // Permet d’afficher la progress bar
    }

    public ArrayList<Succes> getSuccesRegionauxNonValide(long idRegion)
    {
        return getSucces(SuccesBDD.TABLE_ID_REGION + " = ? AND " + SuccesBDD.TABLE_EST_VALIDE + " = ?", new String[] {String.valueOf(idRegion), String.valueOf(SuccesManager.EstNonValide)});
    }

    public static String[] getStructuredCondition(Succes succes)
    {
        return succes.getConditions().split(",");
    }

    public ArrayList<Succes> verifierSuccesRegionaux(long idRegion)
    {
        ArrayList<Succes> succesRegionaux = getSuccesRegionauxNonValide(idRegion);
        return verifierSucces(succesRegionaux);
    }

    private ArrayList<Succes> verifierSucces(ArrayList<Succes> listeSucces) {
        LigneCtrl ligneControlleur;
        if(this.contexte == null)
        {
            ligneControlleur = new LigneCtrl(this.bdd);
        } else {
            ligneControlleur = new LigneCtrl(this.contexte);
        }

        ArrayList<Succes> succesAValider = new ArrayList<Succes>();
        for(Succes succes : listeSucces)
        {
            switch(succes.getType())
            {
                case SuccesManager.TypeLigneRegion:
                {
                    String[] conditions = SuccesCtrl.getStructuredCondition(succes);
                    boolean estSuccesValide = true;
                    for(String idLigneCondition: conditions)
                    {
                        Ligne ligneAVerifier = ligneControlleur.get(idLigneCondition);
                        if(!ligneControlleur.estTotalementTamponnee(ligneAVerifier.getId()))
                        {
                            estSuccesValide = false;
                            break;
                        }
                    }

                    if(estSuccesValide)
                    {
                        succesAValider.add(succes);
                    }
                }
                break;
            }
        }
        return succesAValider;
    }
}
