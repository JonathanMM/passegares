package fr.nocle.passegares.gares;

import android.location.Location;

public class Coordonnee {
    private long idGare;
    private double latitude;
    private double longitude;

    public Coordonnee(long idGare, double latitude, double longitude) {
        this.idGare = idGare;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getIdGare() {
        return idGare;
    }

    public void setIdGare(long idGare) {
        this.idGare = idGare;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Location getLocation()
    {
        Location location = new Location("GPS");

        location.setLongitude(this.longitude);
        location.setLatitude(this.latitude);

        return location;
    }
}
