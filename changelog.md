Version 1.4.5

- Correction d'un problème de déploiement de mise à jour
- Correction d'un problème lors de la première ouverture après une mise à jour

Version 1.4.5

- Fix issue on previous update
- Fix issue on first launch after an update
