package fr.nocle.passegares.succes;

/**
 * Created by user on 26/08/2017.
 */

public class Succes {
    private long id;
    private int type;
    private String label;
    private boolean estValide;
    private boolean estAffiche;
    private int progression;
    private int objectif;
    private String conditions;

    public Succes(long id, int type, String label, boolean estValide, boolean estAffiche, int objectif, String conditions) {
        this.id = id;
        this.type = type;
        this.label = label;
        this.estValide = estValide;
        this.estAffiche = estAffiche;
        this.objectif = objectif;
        this.conditions = conditions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isEstValide() {
        return estValide;
    }

    public void setEstValide(boolean estValide) {
        this.estValide = estValide;
    }

    public boolean isEstAffiche() {
        return estAffiche;
    }

    public void setEstAffiche(boolean estAffiche) {
        this.estAffiche = estAffiche;
    }

    public int getProgression() {
        return progression;
    }

    public void setProgression(int progression) {
        this.progression = progression;
    }

    public int getObjectif() {
        return objectif;
    }

    public void setObjectif(int objectif) {
        this.objectif = objectif;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
}
