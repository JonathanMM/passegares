Version en développement :
[![build status](https://framagit.org/JonathanMM/passegares/badges/master/build.svg)](https://framagit.org/JonathanMM/passegares/commits/master)

Version stable (présente sur le play store) :
[![build status](https://framagit.org/JonathanMM/passegares/badges/stable/build.svg)](https://framagit.org/JonathanMM/passegares/commits/stable)

À récupérer sur les stores :

- Play Store : https://play.google.com/store/apps/details?id=fr.nocle.passegares
- F-droid : https://f-droid.org/fr/packages/fr.nocle.passegares/

# Présentation

PasseGares est une application android ayant pour contexte les gares et stations de transport en commun. Il permet de lister l'ensemble des gares où on est
passé, en les tamponnant quand on se trouve à moins de 150m d'elles.

Une partie jeu de gestion est en cours de création autour de ce concept.

Elle fonctionne avec les gares de France, de certains pays Européens, ainsi que certains réseaux urbains. Néanmoins, vous pouvez aider à l'intégration de
données pour d'autres pays. Vous pouvez également contribuer en ajoutant les données sur les lignes TER, en corrigeant des fautes d'orthographes, en traduisant
l'application. Tous les détails sont disponible dans le fichier CONTRIBUTING.md

# Installation

Cette application est compilé avec android-studio.

# Contribution

Cette application est ouverte aux contributions.

# Éditeur de données

Un éditeur de données est disponible sur http://passegares.nocle.fr Il permet d'afficher les données présentes dans l'application, ainsi que de les modifier :
Il génère les fichiers nécessaires pour une région, assistant ainsi la création de merge request.

Le code source de l'éditeur est disponible sur le dépôt git du projet : https://framagit.org/JonathanMM/passegares-editeur

# Crédits

Les logos et sources de données appartiennent à leur propriétaire respectifs, et ne sont pas compris dans le cadre de la licence de ce logiciel. Vous pouvez les
retrouver dans le fichier Preference.java (le code est toujours à jour :) ).

# Contact

Vous pouvez poster une requête de bug, ou envoyer un courriel à : passegares[@]nocle.fr
