package fr.nocle.passegares.gares;

import fr.nocle.passegares.bdd.GareBDD;

public class CoordonneeBDD {
    public static final String TABLE_CLE = "id";
    public static final String TABLE_ID_GARE = "idGare";
    public static final String TABLE_LATITUDE = "latitude";
    public static final String TABLE_LONGITUDE = "longitude";

    public static final String TABLE_NOM = "Coordonnees";
    public static final String TABLE_CREATION =
            "CREATE TABLE " + TABLE_NOM + " (" +
                    TABLE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TABLE_ID_GARE + " INTEGER, " +
                    TABLE_LONGITUDE + " REAL, " +
                    TABLE_LATITUDE + " REAL" +
                    ");";
    public static final String TABLE_INIT =
            "INSERT INTO " + TABLE_NOM + " (" + TABLE_ID_GARE + ", " + TABLE_LONGITUDE + ", " + TABLE_LATITUDE + ") " +
            "SELECT " + GareBDD.TABLE_CLE + ", " + GareBDD.TABLE_LONGITUDE + ", " + GareBDD.TABLE_LATITUDE +
            " FROM " + GareBDD.TABLE_NOM + ";";
    public static final String TABLE_SUPPRESSION = "DROP TABLE IF EXISTS " + TABLE_NOM + ";";
}

