import tools
import os
import csv

separator = ';'
print("Séparateur utilisé : " + separator)
dialect = csv.unix_dialect
dialect.delimiter = separator
encodingFile = 'utf-8-sig'
regionGare = 'Paris'
scriptFolder = os.path.dirname(os.path.realpath(__file__)) + "/"
regionPath = scriptFolder + '../app/src/main/assets/'+regionGare+'/'

listeGares = []
with open(regionPath + "Coordonnees.csv", "r", encoding='utf8') as fichiersCoordonnees:
    fData = csv.reader(fichiersCoordonnees, dialect)
    titre = next(fData)
    posCol = tools.getEntetesList(titre)
    for contenu in fData:
        idGare = contenu[posCol["idGare"]]
        if not idGare in listeGares:
            listeGares.append(idGare)

# On commence par charger les gares
ligneGares = []
with open(regionPath + "Gares.csv", "r", encoding='utf8') as fichiersGares:
    fData = csv.reader(fichiersGares, dialect)
    titre = next(fData)
    posCol = tools.getEntetesList(titre)
    ligneGares.append(";".join(titre))
    for contenu in fData:
        idGare = contenu[posCol["idExterne"]]
        if idGare in listeGares:
            contenu[posCol["vMaj"]] = "179"
        ligneGares.append(";".join(contenu))

with open(regionPath + "Gares.csv", "w", encoding='utf8') as fichiersGares:
    for ligne in ligneGares:
        fichiersGares.write(ligne + "\n")
