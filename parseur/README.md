# Prérequis

```bash
pip install pyyaml
```

Conversion Lambert 93 en WGS84 (nécessaire pour les données IdFM) : pip install pyproj

# Lancer

```bash
python3 genererData.py
```
