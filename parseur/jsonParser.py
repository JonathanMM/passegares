import tools, os
import json
from regionManager import RegionManager

class jsonParser:
  def __init__(self, config, metadonnees):
    self.config = config
    self.metadonnees = metadonnees
    self.tmpFileName = "data.json"

  def parse(self):
    # Étape 0bis : On regarde ce qu’on va importer
    importerLigne = True
    if 'importType' in list(self.config.keys()):
        importerLigne = tools.getValueOrDefault('lignes', self.config['importType'], True)

    # Étape 1 : Téléchargement du fichier
    if not 'localFile' in list(self.config.keys()) or not self.config['localFile']:
        lien = self.config['link_download']
        tools.telechargerFichier(lien, self.tmpFileName)
    
    dossierId = tools.getValue('dossierId', {}, self.config['default_value'])
    region = dossierId
    regionMgt = RegionManager(self.config, dossierId, region, self.metadonnees)

    with open(self.tmpFileName, 'r') as fichierData:
        data = json.load(fichierData)
        for station in data["features"]:
            coordonnees = station['geometry']['coordinates']
            gare = {
                'idExterne': station['properties']['GlobalID'],
                'nom': station['properties']['NOME'],
                'longitude': coordonnees[0],
                'latitude': coordonnees[1]
            }
            gare['lignes'] = [{'id': "Ligne_"+self.config['prefixIdExterne']+"_"+nom, 'nom': nom} for nom in station['properties']['LINHA'].split('/')]

            regionMgt.addGare(gare)
            
        regionMgt.save()