# Politique de confidentialité (français)

Cette application a été développée par JonathanMM <jonathanmm@free.fr>

Cette application enregistre la localisation ainsi que la date courante lorsque vous appuyez sur le bouton tamponer.
Ces informations sont stockés dans une base de données qui reste intégralement sur votre appareil et ne sont jamais envoyé ailleurs.
Lorsque vous exporter la base de données, ces informations sont stockés dans le fichier généré.

Cette application ne communique pas sur internet, à aucun moment que ce soit.
Elle contient des liens permettant d'accéder à des pages externes, mais aucune information personnelle n'est envoyée via ces liens.

Pour toute demande ou question, vous pouvez directement contacter le développeur JonathanMM à l'adresse <jonathanmm@free.fr>

# Privacy policy (english)

This application was developed by JonathanMM <jonathanmm@free.fr>

This application logs the location as well as the current date when you press the stamp button.
This information is stored in a database that remains entirely on your device and is never sent anywhere.
When you export the database, this information is stored in the generated file.

This application doesn't communicate on internet at any time.
It contains links to external pages, but any personal information is sent via these links.

For any requests or questions, you can directly contact the developer JonathanMM at <jonathanmm@free.fr>