import os
import tools


class RegionManager:
    def __init__(self, config, dossierId, region, metadonnees):
        self.regionsFichier = {}
        self.metadonnes = metadonnees
        self.config = config
        self.dossierId = dossierId
        self.regionName = region
        self.initFichier()

    def initFichier(self):
        scriptFolder = os.path.dirname(os.path.realpath(__file__)) + "/"
        regionGare = self.dossierId
        if not regionGare in self.regionsFichier.keys():  # Pas encore en cache
            # Étape 0bis : On regarde ce qu’on va importer
            self.importerLigne = True
            if 'importType' in list(self.config.keys()):
                self.importerLigne = tools.getValueOrDefault('lignes', self.config['importType'], True)

            # On regarde si le dossier existe ou pas
            regionPath = scriptFolder + '../app/src/main/assets/'+regionGare+'/'
            if not os.path.isdir(regionPath):
                # On crée le dossier
                os.mkdir(regionPath)
                # On crée les fichiers
                self.regionsFichier[regionGare] = {
                    'gares': open(regionPath+'Gares.csv', 'a+'),
                    'lignes': open(regionPath+'Lignes.csv', 'a'),
                    'gdl': open(regionPath+'GaresDansLigne.csv', 'a'),
                    'garesData': {},
                    'idGares': [],
                    'idLignes': [],
                    'garesDansLigneAssociation': {},
                    'path': regionPath
                }
                # Et on va leur mettre leur entêtes
                self.regionsFichier[regionGare]['gares'].write(
                    "id;idExterne;nom;exploitant;latitude;longitude;couleur;couleurEvolution;vCreation;vMaj;vSuppression\n")
                self.regionsFichier[regionGare]['lignes'].write(
                    "idExterne;nom;type;ordre;couleur;vCreation;vMaj;vSuppression\n")
                self.regionsFichier[regionGare]['gdl'].write(
                    "idGare;idLigne;ordre;PDLFond;PDLPoint;vCreation;vMaj;vSuppression\n")
            else:
                # On récupère les trois fichiers
                self.regionsFichier[regionGare] = {
                    'gares': open(regionPath+'Gares.csv', 'r+'),
                    'lignes': open(regionPath+'Lignes.csv', 'r+'),
                    'gdl': open(regionPath+'GaresDansLigne.csv', 'a'),
                    'garesDansLigneAssociation': {},
                    'path': regionPath
                }
                # Et on génère le tableau idGares et idLignes
                self.regionsFichier[regionGare]['idGares'] = self.__getAllIdExternes(
                    self.regionsFichier[regionGare]['gares'])
                self.regionsFichier[regionGare]['idLignes'] = self.__getAllIdExternes(
                    self.regionsFichier[regionGare]['lignes'])
                self.regionsFichier[regionGare]['garesData'] = self.__getAllData(
                    self.regionsFichier[regionGare]['gares'])

            if self.config['substitue'] != None and 'dossierId' in self.config['substitue'] and regionGare in self.config['substitue']['dossierId'].keys():
                regionGareAlt = self.config['substitue']['dossierId'][regionGare]
            else:
                regionGareAlt = regionGare

            # Si ligne unique, on la crée a ce moment
            if self.importerLigne and not self.config['lignes']:
                idLigneUnique = self.config['prefixIdExterne'] + \
                    "_"+regionGareAlt+"_U"
                if not idLigneUnique in self.regionsFichier[regionGare]['idLignes']:
                    self.regionsFichier[regionGare]['lignes'].write(
                        idLigneUnique + ";Ligne Unique;"+self.config['lignesType']+";0;#000000;"+str(self.metadonnes)+";0;0\n")
                    self.regionsFichier[regionGareAlt]['idLignes'].append(
                        idLigneUnique)

            # On va préparer l'objet d'association Gare et Ligne
            if self.importerLigne:
                for idLigne in self.regionsFichier[regionGareAlt]['idLignes']:
                    self.regionsFichier[regionGareAlt]['garesDansLigneAssociation'][idLigne] = [
                    ]

            # On rempli également le fichier des régions
            fRegions = open(scriptFolder + '../app/src/main/assets/Regions.csv', 'r+')
            dataRegions = self.__getIdRegions(fRegions)
            idRegions = dataRegions['idExternes']
            if not self.regionName in idRegions.keys():
                # Il nous faut l'id max (dernière région)
                idNewRegion = self.__nextIdRegion(fRegions)
                fRegions.write(str(idNewRegion)+";"+self.regionName +
                               ";"+regionGare+";"+str(self.metadonnes)+";0"+"\n")
            else:
                numLigne = idRegions[self.regionName]
                lignes = dataRegions['lignes']
                dataLigne = self.__getDataLigneRegion(
                    lignes[numLigne], dataRegions['entetes'])
                vMaj = self.metadonnes
                if dataLigne['vCreation'] == str(self.metadonnes):
                    vMaj = 0 # Si c'est la même version que la création, rien ne sert de la changer.
                lignes[numLigne] = str(dataLigne['id'])+";"+dataLigne['nom']+";"+dataLigne['dossierId'] + \
                    ";"+str(dataLigne['vCreation']) + \
                    ";"+str(vMaj)+"\n"
                # On nettoie le fichier
                fRegions.seek(0, 0)
                fRegions.truncate()
                # Et on écrit
                fRegions.writelines(lignes)
            fRegions.close()

            # On récupère également les entêtes
            self.regionsFichier[regionGare]['gares'].seek(0, 0)
            self.regionsFichier[regionGare]['enteteGare'] = tools.getEntetes(
                self.regionsFichier[regionGare]['gares'].readline())
            # Et on remet le curseur à sa place
            self.regionsFichier[regionGare]['gares'].seek(0, 2)

    def getFichierRegion(self):
        return self.regionsFichier[self.dossierId]

    def addGare(self, gare):
        # On va regarder la région
        regionGare = tools.getValue(
            'dossierId', gare, self.config['default_value'])
        gare['region'] = regionGare
        # A-t-elle une valeur normalisée ?
        if self.config['substitue'] != None and 'dossierId' in self.config['substitue'] and regionGare in self.config['substitue']['dossierId'].keys():
            regionGare = self.config['substitue']['dossierId'][regionGare]
        gare['dossierId'] = regionGare

        if len(regionGare) > 0 and (self.config['ignore_values'] == None or not 'dossierId' in self.config['ignore_values'] or not regionGare in self.config['ignore_values']['dossierId']):
            # On récupère le fichier de la région
            fRegionInfos = self.getFichierRegion()

            if gare['idExterne'] == '__auto__':
                # On doit en autogénéré un
                gare['id'] = self.config['prefixIdExterne'] + f"{(len(fRegionInfos['idGares']) + 1):06}"
                gare['idExterne'] = gare['id']
            else:
                # On ajoute le prefixe à l'Id
                gare['id'] = self.config['prefixIdExterne']+gare['idExterne']

            if self.config['substitue'] != None and 'idExterne' in self.config['substitue'] and gare['idExterne'] in self.config['substitue']['idExterne']:
                gare['idExterne'] = str(
                    self.config['substitue']['idExterne'][gare['idExterne']])
                gare['id'] = gare['idExterne'] # Potentiellement différent

            # On ne traite que les nouvelles gares
            if not gare['idExterne'] in fRegionInfos['idGares']:
                print('nouvelle gare : ' + gare['nom'])
                fRegionInfos['idGares'].append(gare['idExterne'])
                gare['exploitant'] = tools.getValue(
                    'exploitant', gare, self.config['default_value'])
                gare['surTitre'] = tools.getValue(
                    'surTitre', gare, self.config['default_value'])
                gare['sousTitre'] = tools.getValue(
                    'sousTitre', gare, self.config['default_value'])
                # On va récupérer la couleur et la couleur évolution
                gare['couleur'] = tools.getCouleur()
                gare['couleurEvolution'] = tools.getCouleurEvolution(
                    gare['nom'], gare['couleur'])
                # Si on a des coordonnees, on va les traiter
                tools.getCoordonnees(gare)

                # On note les métadonnées
                gare['vCreation'] = str(self.metadonnes)
                gare['vMaj'] = '0'
                gare['vSuppression'] = '0'
                # On rempli le fichier Gares
                # fRegionInfos['gares'].write(gare['id']+';'+gare['idExterne']+';'+tools.nettoyerTexte(gare['nom'])+
                #                             ';'+gare['exploitant']+';'+str(gare['latitude']) +
                #                             ';'+str(gare['longitude'])+';'+str(gare['couleur'])+
                #                             ';'+str(gare['couleurEvolution'])+';'+str(self.metadonnes)+';0;0'+"\n")
                fRegionInfos['garesData'][gare['id']] = gare

            if self.importerLigne:
                lignesGare = []
                if not self.config['lignes']:  # Ligne unique
                    lignesGare.append(
                        {'id': self.config['prefixIdExterne']+"_"+regionGare+"_U", 'nom': "Ligne Unique"})
                elif 'lignes' in gare:
                    if type(gare['lignes']) in (tuple, list): # C'est un tableau
                        lignesGare = gare['lignes']
                    else: # C'est un string
                        listeLignesId = [gare['lignes']]
                        if "," in gare['lignes']:
                            listeLignesId = gare['lignes'].split(',')
                        lignesGare = map(lambda id: {'id': self.config['prefixIdExterne']+id, 'nom': id}, listeLignesId)

                # On regarde pour la ligne maintenant
                for infoLigne in lignesGare:
                    idLigne = self.__nettoyerId(infoLigne['id'])
                    if not idLigne in fRegionInfos['idLignes']:
                        fRegionInfos['idLignes'].append(idLigne)
                        fRegionInfos['garesDansLigneAssociation'][idLigne] = []
                        # On va créer la ligne
                        fRegionInfos['lignes'].write(
                            idLigne + ";"+infoLigne['nom']+";"+self.config['lignesType']+";0;#000000;"+str(self.metadonnes)+";0;0\n")

                    if not gare['id'] in fRegionInfos['garesDansLigneAssociation'][idLigne]:
                        fRegionInfos['garesDansLigneAssociation'][idLigne].append(
                            gare['id'])

    def updateGare(self, gare):
        # On va mettre à jour la gare si nécessaire
        print("Mise à jour ? : " + gare['nom'])
        fRegionInfos = self.getFichierRegion()
        gareData = fRegionInfos['garesData'][str(gare['idExterne'])]
        gare['vCreation'] = gareData['vCreation']
        gare['vMaj'] = gareData['vMaj']
        gare['vSuppression'] = gareData['vSuppression']
        if int(gareData['vSuppression']) == 0 and float(gareData['longitude']) != float(gare['longitude']) or float(gareData['latitude']) != float(gare['latitude']):
            print("Mise à jour nécessaire : " + gare['nom'])
            gareData['longitude'] = gare['longitude']
            gareData['latitude'] = gare['latitude']
            gareData['vMaj'] = str(self.metadonnes)
        if int(gareData['vSuppression']) == 0 and \
            (('sousTitre' in gareData and gareData['sousTitre'] != gare['sousTitre']) or \
            ('surTitre' in gareData and gareData['surTitre'] != gare['surTitre']) or \
            (gareData['nom'] != gare['nom'])):
            print("Mise à jour nécessaire : " + gare['nom'])
            gareData['nom'] = gare['nom']
            gareData['surTitre'] = gare['surTitre']
            gareData['sousTitre'] = gare['sousTitre']
            gareData['vMaj'] = str(self.metadonnes)
        else:
            gareData['surTitre'] = None
            gareData['sousTitre'] = None

    def save(self):
        # Étape 4 : Post traitement
        regionFichier = self.getFichierRegion()
        for idLigne, listeGares in regionFichier['garesDansLigneAssociation'].items():
            # Étape 4b : On rempli le fichier GaresDansLigne
            for idGare in listeGares:
                regionFichier['gdl'].write(
                    idGare+";"+idLigne+";0;0;0;"+str(self.metadonnes)+";0;0\n")

        # Étape 4c : On écrit le fichier Gares
        regionFichier['gares'].close()

        with open(regionFichier['path'] + 'Gares.csv', 'w') as fichier:
            fichier.write(
                "id;idExterne;nom;surTitre;sousTitre;exploitant;latitude;longitude;couleur;couleurEvolution;vCreation;vMaj;vSuppression\n")

            # Puis, on écrit chaque gare
            for _, gare in regionFichier['garesData'].items():
                surTitre = None
                if 'surTitre' in gare:
                    surTitre = gare['surTitre']
                sousTitre = None
                if 'sousTitre' in gare:
                    sousTitre = gare['sousTitre']
                fichier.write(gare['id']+';'+gare['idExterne']+';'+tools.nettoyerTexte(gare['nom'])+
                                            ';'+tools.nettoyerTexte(surTitre)+';'+tools.nettoyerTexte(sousTitre)+
                                            ';'+gare['exploitant']+';'+str(gare['latitude']) +
                                            ';'+str(gare['longitude'])+';'+str(gare['couleur'])+
                                            ';'+str(gare['couleurEvolution'])+';'+str(gare['vCreation'])+
                                            ';'+str(gare['vMaj'])+';'+str(gare['vSuppression'])+''+"\n")

        # Étape 4d : On ferme les fichiers
        regionFichier['lignes'].close()
        regionFichier['gdl'].close()

    def __nextIdRegion(self, fRegions):
        idMaxRegion = 0
        i = 0
        posIdRegion = 0
        fRegions.seek(0)
        for ligne in fRegions.readlines():
            contenu = ligne.strip().split(';')
            if i == 0:  # entête
                j = 0
                for c in contenu:
                    if c == 'id':
                        posIdRegion = j
                        break
                    j += 1
            else:
                if idMaxRegion < int(contenu[posIdRegion]):
                    idMaxRegion = int(contenu[posIdRegion])
            i += 1
        return idMaxRegion + 1

    def __getDataLigneRegion(self, ligne, entetes):
        dataContenu = {}
        contenu = ligne.strip().split(';')
        i = 0
        for c in contenu:
            dataContenu[entetes[i]] = c
            i += 1
        return dataContenu

    def __getIdRegions(self, fichier):
        idExterneKey = 'dossierId'
        idExternes = {}
        entetes = {}
        i = 0
        posIdExterne = 0
        # On doit récupérer toutes les lignes pour pouvoir les écrire ensuite
        lignes = fichier.readlines()
        for ligne in lignes:
            contenu = ligne.strip().split(';')
            if ligne == "":
                continue
            if i == 0:  # entête
                j = 0
                for c in contenu:
                    if c == idExterneKey:
                        posIdExterne = j
                    entetes[j] = c
                    j += 1
            else:
                idExternes[contenu[posIdExterne]] = i
            i += 1
        return {'idExternes': idExternes, 'lignes': lignes, 'entetes': entetes}

    def __getAllIdExternes(self, fichier):
        idExternes = []
        idExterneKey = 'idExterne'
        i = 0
        posId = 0
        for ligne in fichier.readlines():
            contenu = ligne.strip().split(';')
            if i == 0:  # entête
                j = 0
                for c in contenu:
                    if c == idExterneKey:
                        posId = j
                        break
                    j += 1
            else:
                idExternes.append(contenu[posId])
            i += 1
        return idExternes

    def __getAllData(self, fichier):
        gares = {}
        entetes = {}
        i = 0
        fichier.seek(0) # On revient au début du fichier
        for ligne in fichier.readlines():
            contenu = ligne.strip().split(';')
            if i == 0:  # entête
                entetes = tools.getEntetes(ligne)
            else:
                gare = {}
                for c, i in entetes.items():
                    gare[c] = contenu[i]
                gares[str(gare['idExterne'])] = gare
            i += 1
        return gares

    def __nettoyerId(self, idExterne):
        return idExterne.replace('&', '').replace(' ', '')
