import tools
import zipfile
import csv
import os
from regionManager import RegionManager

class gtfsParser:
  def __init__(self, config, metadonnees):
    self.config = config
    self.metadonnees = metadonnees

  def getLigneType(self, ligneType):
    if ligneType == 0:
      return 'Tramway'
    elif ligneType == 1:
      return 'Metro'
    elif ligneType == 2:
      return 'Train'
    elif ligneType == 7:
      return 'Funiculaire'

  def parse(self):
    scriptFolder = os.path.dirname(os.path.realpath(__file__)) + "/"
    #Étape 1 : On commence par télécharger l'archive
    tmpFileName = scriptFolder+'gtfs.zip'
    if not 'localFile' in list(self.config.keys()) or not self.config['localFile']:
      lien = self.config['link_download']
      tools.telechargerFichier(lien, tmpFileName) #Désactivé pour les tests

    #Étape 2 : On dezippe
    tmpDirName = scriptFolder+'gtfs/'
    zip_ref = zipfile.ZipFile(tmpFileName, 'r') #Désactivé pour les tests
    zip_ref.extractall(tmpDirName) #Désactivé pour les tests
    zip_ref.close() #Désactivé pour les tests

    #Étape 3 : On commence par les lignes
    print('Récupération des lignes')
    cheminFichier = tmpDirName + "routes.txt"
    routesFile = open(cheminFichier, 'r', encoding='utf-8-sig')

    #Étape 3a : On récupère ses entêtes
    lignesFichier = csv.reader(routesFile)
    separateur = ','
    titre = separateur.join(lignesFichier.__next__())
    posCol = tools.getEntetes(titre, None, separateur)
    print('Liste des entêtes : ')
    print(posCol)

    #Étape 3b : On regarde les lignes qui nous intéresse
    lignes = {}
    region = self.config['default_value']['dossierId']
    regionMgt = RegionManager(self.config, region, region, self.metadonnees)
    fRegionInfos = regionMgt.getFichierRegion()
    for contenu in lignesFichier:
      row = {}
      for c,i in posCol.items():
        row[c] = tools.nettoyerTexte(contenu[i])
      
      #On ne gare que les types Tram (0), Métro (1), Trains (2) et Funiculaires (7)
      routeType = int(row['route_type'])
      if routeType == 0 or routeType == 1 or routeType == 2 or routeType == 7:
        idLigne = row['route_id']
        if 'route_color' in row.keys():
          couleur = row['route_color']
          if couleur == "":
            couleur = "#000000"
          elif couleur[0] != "#":
            couleur = "#" + couleur
        else:
          couleur = '#000000'
        lignes[idLigne] = {'nom': row['route_short_name'], 'couleur': couleur, 'trips': [], 'stops': []}
        #Et on la créer dans le fichier
        idLigneExterne = ("Ligne_"+self.config['prefixIdExterne']+idLigne).strip()

        if not idLigneExterne in fRegionInfos['idLignes']:
          fRegionInfos['idLignes'].append(idLigneExterne)
          fRegionInfos['garesDansLigneAssociation'][idLigneExterne] = []
          #On va créer la ligne
          fRegionInfos['lignes'].write(idLigneExterne+ ";"+lignes[idLigne]['nom']+";"+self.getLigneType(routeType)+";0;"+lignes[idLigne]['couleur']+";"+str(tools.metadonnes)+";0;0\n")
          print("Ajout de la ligne " + idLigneExterne + " ("+lignes[idLigne]['nom']+")")
        lignes[idLigne]['id'] = idLigneExterne
    routesFile.close()
    print('Récupération des lignes terminées')

    #Étape 4 : On passe aux parcours
    print('Récupération des trips')
    cheminFichier = tmpDirName + "trips.txt"
    tripsFile = open(cheminFichier, 'r', encoding='utf-8-sig')
    
    #Étape 4a : On récupère ses entêtes
    titre = tripsFile.readline()
    separateur = ','
    posCol = tools.getEntetes(titre, None, separateur)
    print('Liste des entêtes : ')
    print(posCol)

    #Étape 4b : On regarde les lignes qui nous intéresse
    for ligne in tripsFile.readlines():
      contenu = ligne.strip().split(separateur)
      row = {}
      for c,i in posCol.items():
        row[c] = tools.nettoyerTexte(contenu[i])

      #On regarde si le trip nous intéresse
      if row['route_id'] in list(lignes.keys()):
        lignes[row['route_id']]['trips'].append(row['trip_id'])

    tripsFile.close()
    print('Récupération des trips terminé')

    # Aide pour le développement
    # print("Nombre de trips")
    # totalTrip = 0
    # for ligneId in lignes:
    #   ligne = lignes[ligneId]
    #   print("Ligne " + ligneId + ": " + str(len(ligne['trips'])))
    #   totalTrip += len(ligne['trips'])
    # print("Total : " + str(totalTrip))
    # return None

    #Étape 5 : Le plus dur maintenant, les horaires !
    stopsId = []
    print('Récupération des stop_times')
    cheminFichier = tmpDirName + "stop_times.txt"
    stopTimesFile = open(cheminFichier, 'r', encoding='utf-8-sig')
    
    #Étape 5a : On récupère ses entêtes
    titre = stopTimesFile.readline()
    separateur = ','
    posCol = tools.getEntetes(titre, None, separateur)
    print('Liste des entêtes : ')
    print(posCol)

    #Étape 5b : On regarde les arrêts qui nous intéresse
    currentTrip = None
    isInterrestingTrip = False
    currentLines = []
    for ligneBrut in stopTimesFile.readlines():
      contenu = ligneBrut.strip().split(separateur)
      row = {}
      for c,i in posCol.items():
        row[c] = tools.nettoyerTexte(contenu[i])

      tripId = row['trip_id']
      if currentTrip != tripId:
        currentTrip = tripId
        isInterrestingTrip = False
        currentLines = []
        #On regarde si l'horaire fait parti d'un de nos trips
        for ligneId in lignes:
          ligne = lignes[ligneId]
          if not row['trip_id'] in ligne['trips']:
            continue
          
          currentLines.append(ligneId)
          isInterrestingTrip = True
        
      #On a un arrêt ! On l'ajoute à la liste des arrêts
      if isInterrestingTrip:
        stopId = row['stop_id']
        if not stopId in stopsId:
          stopsId.append(stopId)

        for ligneId in currentLines:
          ligne = lignes[ligneId]
          if not stopId in ligne['stops']:
            ligne['stops'].append(stopId)

    stopTimesFile.close()
    print('Récupération des stop_times terminé')
    
    # Aide pour le développement
    # print("Nombre de stops")
    # totalStops = 0
    # for ligneId in lignes:
    #   ligne = lignes[ligneId]
    #   print("Ligne " + ligneId + ": " + str(len(ligne['stops'])))
    #   totalStops += len(ligne['stops'])
    # print("Total : " + str(totalStops))
    # return None

    #Étape 6 : On fini par les arrêts, c'est l'heure du recoupement !
    print('Récupération des arrêts')
    cheminFichier = tmpDirName + "stops.txt"
    stopsFile = open(cheminFichier, 'r', encoding='utf-8-sig')
    
    #Étape 6a : On récupère ses entêtes
    lignesFichier = csv.reader(stopsFile)
    separateur = ','
    titre = separateur.join(lignesFichier.__next__())
    #On va faire la correspondance entre le format GTFS et notre format
    header = {'stop_id': 'idExterne', 'stop_name': 'nom', 'stop_lat': 'latitude', 'stop_lon': 'longitude', 'parent_station': 'parent_station', 'location_type': 'location_type'}
    posCol = tools.getEntetes(titre, header, separateur)
    print('Liste des entêtes : ')
    print(posCol)

    #Étape 6b : On regarde les arrêts qui nous intéresse
    stopArea = {}
    stopPoint = {}
    stopMixed = {}
    for contenu in lignesFichier:
      gare = {}
      for c,i in posCol.items():
        gare[c] = tools.nettoyerTexte(contenu[i])

      if not 'parent_station' in gare.keys():
        gare['parent_station'] = ""

      #C'est gagné, on a un arrêt ! Soit il y a qu'un seul type d'arrêt
      if not 'location_type' in gare or ('ignoreLocationType' in list(self.config.keys()) and self.config['ignoreLocationType']):
        if gare['idExterne'] in stopsId:
          stopPoint[gare['idExterne']] = [gare['idExterne']]
          stopArea[gare['idExterne']] = gare
      else:
        # Soit on a ou un area (type = 1), ou un stopPoint intéressant
        if len(gare['location_type']) > 0 and int(gare['location_type']) == 1:
          stopArea[gare['idExterne']] = gare
          if 'stationsWithAndWithoutStepArea' in self.config and self.config['stationsWithAndWithoutStepArea']:
            if gare['nom'] in stopMixed:
              # Ça existe déjà, il va falloir mettre à jour les idExterne
              previousIdExterne = stopMixed[gare['nom']]
              del stopArea[previousIdExterne]
              if gare['idExterne'] in stopPoint:
                stopPoint[gare['idExterne']] = stopPoint[gare['idExterne']] + stopPoint[previousIdExterne]
                del stopPoint[previousIdExterne]
              elif previousIdExterne in stopPoint:
                stopPoint[gare['idExterne']] = stopPoint[previousIdExterne] + [gare['idExterne']]
                del stopPoint[previousIdExterne]
              else:
                stopPoint[gare['idExterne']] = [gare['idExterne']]
            
            if gare['idExterne'] in stopsId:
              stopMixed[gare['nom']] = gare['idExterne']
        elif gare['idExterne'] in stopsId:
          if 'stationsWithAndWithoutStepArea' in self.config and self.config['stationsWithAndWithoutStepArea'] and gare['parent_station'] == "":
            # Voilà, on a des données pourris, on va tenter de regrouper sur le nom
            if gare['nom'] in stopMixed.keys():
              stopPoint[stopMixed[gare['nom']]].append(gare['idExterne'])
            else:
              stopArea[gare['idExterne']] = gare
              stopPoint[gare['idExterne']] = [gare['idExterne']]
              stopMixed[gare['nom']] = gare['idExterne']
          if not gare['parent_station'] in stopPoint.keys():
            stopPoint[gare['parent_station']] = []

          stopPoint[gare['parent_station']].append(gare['idExterne'])
    stopTimesFile.close()

    #Étape 6c: Maintenant, il n'y a plus qu'à s'occuper des arrêts
    for gareId, gare in stopArea.items():
      #On regarde si l'arrêt est référencé dans une ligne
      if not gareId in stopPoint.keys():
        continue
      #On va regarder la région
      regionGare = tools.getValue('dossierId', gare, self.config['default_value'])
      gare['region'] = regionGare
      #A-t-elle une valeur normalisée ?
      if self.config['substitue'] != None and 'dossierId' in self.config['substitue'] and regionGare in self.config['substitue']['dossierId'].keys():
        regionGare = self.config['substitue']['dossierId'][regionGare]
      gare['dossierId'] = regionGare
          
      if len(regionGare) > 0 and (self.config['ignore_values'] == None or not regionGare in self.config['ignore_values']['dossierId']):
        gare['lignes'] = []
        for stopPointId in stopPoint[gareId]:
          for ligneId in lignes:
            ligne = lignes[ligneId]
            if stopPointId in ligne['stops']:
              gare['lignes'].append(ligne)
        
        #On ne traite que les nouvelles gares
        if not gare['idExterne'] in fRegionInfos['idGares']:
          regionMgt.addGare(gare)
        else:
          regionMgt.updateGare(gare)

    print('Récupération des stop_times terminé')

    #Étape 7 : Post traitement
    regionMgt.save()
    
    # return None #Juste pour les tests
    #Étape 8 : On efface les fichiers temporaires
    if not 'localFile' in list(self.config.keys()) or not self.config['localFile']:
      os.remove(tmpFileName)
    for root, _, files in os.walk(tmpDirName, topdown=False):
      for name in files:
          os.remove(os.path.join(root, name))
    os.rmdir(tmpDirName)