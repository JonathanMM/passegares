package fr.nocle.passegares.controlleur;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.nocle.passegares.bdd.GareBDD;
import fr.nocle.passegares.bdd.GareDansLigneBDD;
import fr.nocle.passegares.bdd.LigneBDD;
import fr.nocle.passegares.gares.CoordonneeCtrl;
import fr.nocle.passegares.modele.Gare;
import fr.nocle.passegares.modele.Ligne;
import fr.nocle.passegares.modele.Region;
import fr.nocle.passegares.succes.Succes;
import fr.nocle.passegares.succes.SuccesCtrl;

/**
 * Created by jonathanmm on 03/09/16.
 */
public class ImportCSV {
    public static void updateNbGaresDansLigne(SQLiteDatabase bdd, HashMap<Long, Integer> nbGares)
    {
        // Parcours de la Hashmap
        Iterator i = nbGares.entrySet().iterator();

        for(Map.Entry<Long, Integer> item : nbGares.entrySet())
        {
            ContentValues valeur = new ContentValues();
            valeur.put(LigneBDD.TABLE_NB_GARES, item.getValue());

            bdd.update(LigneBDD.TABLE_NOM, valeur, LigneBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(item.getKey())});
        }
    }

    public static void updateNbGaresDansLigne(SQLiteDatabase bdd, Ligne ligne)
    {
        Cursor c = bdd.rawQuery("SELECT COUNT(*) FROM " + GareDansLigneBDD.TABLE_NOM + " WHERE idLigne = ?", new String[]{String.valueOf(ligne.getId())});
        c.moveToFirst();
        ContentValues valeur = new ContentValues();
        valeur.put(LigneBDD.TABLE_NB_GARES, c.getInt(0));
        c.close();
        bdd.update(LigneBDD.TABLE_NOM, valeur, LigneBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(ligne.getId())});
    }

    private static void emptyDataGareDansLigne(SQLiteDatabase bdd)
    {
        // On vide la table
        bdd.execSQL(GareDansLigneBDD.TABLE_SUPPRESSION);
        bdd.execSQL(GareDansLigneBDD.TABLE_CREATION);
    }

    public static void reinitDataGareDansLigne(Context contexte, SQLiteDatabase bdd)
    {
        emptyDataGareDansLigne(bdd);

        RegionCtrl regionCtrl = new RegionCtrl(bdd);
        for(Region r : regionCtrl.getAllRegions(RegionCtrl.REGIONSINSTALLE))
        {
            updateDataGareDansLigne(contexte, bdd, 1, -1, r.getId(), r.getDossierId());
        }
    }

    public static void updateData(Context contexte, SQLiteDatabase bdd, int since, int to)
    {
        ArrayList<Region> listeRegionAImporter =  updatedataRegions(contexte, bdd, since, to);
        if(listeRegionAImporter != null)
        {
            for(Region r : listeRegionAImporter)
            {
                updateAllDataRegion(contexte, bdd, since, to, r);
            }
        }
    }

    public static void updateAllDataRegion(Context contexte, SQLiteDatabase bdd, int since, int to, Region r)
    {
        ArrayList<String> listeGaresTouchees = updateDataGares(contexte, bdd, since, to, r.getDossierId());
        updateDataLignes(contexte, bdd, since, to, r.getId(), r.getDossierId());
        updateDataGareDansLigne(contexte, bdd, since, to, r.getId(), r.getDossierId());
        updateDataSucces(contexte, bdd, since, to, r.getId(), r.getDossierId());
        updateDataCoordonnees(contexte, bdd, r.getDossierId(), listeGaresTouchees);
    }

    public static ArrayList<Region> updatedataRegions(Context contexte, SQLiteDatabase bdd, int since, int to)
    {
        String name = "Regions.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        RegionCtrl regionCtrl = new RegionCtrl(bdd);

        //On prépare la liste des régions pour la mise à jour
        ArrayList<Region> listeRegions = new ArrayList<>();
        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdRegion = -1, posNomRegion = -1, posDossierIdRegion = -1;
            int posVCreation = -1, posVMaj = -1;

            while ((line = br.readLine()) != null)
            {
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("id"))
                            posIdRegion = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomRegion = j;
                        else if(c.equalsIgnoreCase("dossierId"))
                            posDossierIdRegion = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vCreation = Integer.valueOf(cellules[posVCreation]);
                    int vMaj = Integer.valueOf(cellules[posVMaj]);

                    //Infos gare
                    if(since < vCreation && (to == -1 || to >= vCreation))
                    {
                        //Nouvelle donnée
                        //Données de la region
                        Region r = new Region(Long.valueOf(cellules[posIdRegion]),
                                cellules[posNomRegion],
                                false,
                                cellules[posDossierIdRegion]);

                        //On sauvegarde
                        regionCtrl.create(r);

                        //TO FIX : Les régions nouvellement crées ne devraient pas être ajouté directement, à enlever quand moyen sélectionner régions sera fait
                        //listeRegions.add(r);
                    } else if(since < vMaj && to >= vMaj)
                    {
                        //On va récupérer la région
                        Region r = regionCtrl.get(Long.valueOf(cellules[posIdRegion]));

                        if(r == null) //La région n'a pas été trouvé, on la crée
                        {
                            r = new Region(Long.valueOf(cellules[posIdRegion]),
                                    cellules[posNomRegion],
                                    false,
                                    cellules[posDossierIdRegion]);

                            //On sauvegarde
                            regionCtrl.create(r);
                        } else {

                            //On met à jour
                            r.setNom(cellules[posNomRegion]);
                            r.setDossierId(cellules[posDossierIdRegion]);

                            regionCtrl.update(r);

                            if(r.isEstInstalle())
                                listeRegions.add(r);
                        }
                    }
                }
                i++;
            }

            return listeRegions;
        } catch (FileNotFoundException e) {
            Log.e("Import Régions", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Régions", "Problème d'entrée/sortie");
        }
        return null;
    }

    public static ArrayList<String> updateDataGares(Context contexte, SQLiteDatabase bdd, int since, int to, String ville)
    {
        String name = ville + "/" + "Gares.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        GareCtrl gareCtrl = new GareCtrl(bdd);
        CoordonneeCtrl coordonneeCtrl = new CoordonneeCtrl(bdd);
        ArrayList<String> idGaresTouchees = new ArrayList<>();

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdGare = -1, posNomGare = -1, posExploitantGare = -1;
            int posSurTitre = -1, posSousTitre = -1;
            int posLatitudeGare = -1, posLongitudeGare = -1;
            int posCouleur = -1, posCouleurEvo = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("id"))
                            posIdGare = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomGare = j;
                        else if(c.equalsIgnoreCase("surTitre"))
                            posSurTitre = j;
                        else if(c.equalsIgnoreCase("sousTitre"))
                            posSousTitre = j;
                        else if(c.equalsIgnoreCase("exploitant"))
                            posExploitantGare = j;
                        else if(c.equalsIgnoreCase("latitude"))
                            posLatitudeGare = j;
                        else if(c.equalsIgnoreCase("longitude"))
                            posLongitudeGare = j;
                        else if(c.equalsIgnoreCase("couleur"))
                            posCouleur = j;
                        else if(c.equalsIgnoreCase("couleurEvolution"))
                            posCouleurEvo = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    String idGare = cellules[posIdGare];
                    int vCreation = Integer.valueOf(cellules[posVCreation]);
                    int vMaj = Integer.valueOf(cellules[posVMaj]);
                    int vSupprime = Integer.valueOf(cellules[posVSupprime]);

                    //Infos gare
                    if(since < vCreation && (to == -1 || to >= vCreation))
                    {
                        //Nouvelle donnée
                        //Données de la gare
                        Gare g = new Gare(-1, idGare, cellules[posNomGare],
                                Double.valueOf(cellules[posLongitudeGare]),
                                Double.valueOf(cellules[posLatitudeGare]),
                                cellules[posExploitantGare], 0,
                                Integer.valueOf(cellules[posCouleur]),
                                Integer.valueOf(cellules[posCouleurEvo]), 0, null, null);

                        if(posSousTitre != -1 && posSurTitre != -1)
                        {
                            g.setSurTitre(cellules[posSurTitre]);
                            g.setSousTitre(cellules[posSousTitre]);
                        }

                        //On sauvegarde
                        gareCtrl.create(g);
                        // Et on importe les coordonnées principales de la gare
                        coordonneeCtrl.create(g);
                        idGaresTouchees.add(idGare);
                    } else if(since < vMaj && to >= vMaj)
                    {
                        //On va récupérer la gare
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        //On la met à jour
                        g.setNom(cellules[posNomGare]);
                        g.setLongitude(Double.valueOf(cellules[posLongitudeGare]));
                        g.setLatitude(Double.valueOf(cellules[posLatitudeGare]));
                        g.setExploitant(cellules[posExploitantGare]);
                        g.setCouleur(Integer.valueOf(cellules[posCouleur]));
                        g.setCouleurEvo(Integer.valueOf(cellules[posCouleurEvo]));
                        if(posSousTitre != -1 && posSurTitre != -1)
                        {
                            g.setSurTitre(cellules[posSurTitre]);
                            g.setSousTitre(cellules[posSousTitre]);
                        }
                        gareCtrl.update(g);
                        // Pour les coordonnées, on vide tout ce qu’on a sur la gare
                        coordonneeCtrl.delete(g);
                        // Et on importe les coordonnées principales de la gare
                        coordonneeCtrl.create(g);
                        idGaresTouchees.add(idGare);
                    } else if(since < vSupprime && to >= vSupprime)
                    {
                        //On va récupérer la gare
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        //Et on supprime
                        if(g != null) {
                            gareCtrl.delete(g.getId());
                            coordonneeCtrl.delete(g);
                        }
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Gares", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Gares", "Problème d'entrée/sortie");
        }

        return idGaresTouchees;
    }

    public static void updateDataLignes(Context contexte, SQLiteDatabase bdd, int since, int to, long idRegion, String ville)
    {
        String name = ville + "/" + "Lignes.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        LigneCtrl ligneCtrl = new LigneCtrl(bdd);

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdLigne = -1, posNomLigne = -1, posTypeLigne = -1, posOrdreLigne = -1;
            int posCouleurLigne = -1, posVille = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("idExterne"))
                            posIdLigne = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomLigne = j;
                        else if(c.equalsIgnoreCase("type"))
                            posTypeLigne = j;
                        else if(c.equalsIgnoreCase("ordre"))
                            posOrdreLigne = j;
                        else if(c.equalsIgnoreCase("couleur"))
                            posCouleurLigne = j;
                        else if(c.equalsIgnoreCase("ville"))
                            posVille = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vCreation = Integer.valueOf(cellules[posVCreation]);
                    int vMaj = Integer.valueOf(cellules[posVMaj]);
                    int vSupprime = Integer.valueOf(cellules[posVSupprime]);

                    //Infos gare
                    if(since < vCreation && (to == -1 || to >= vCreation))
                    {
                        //Nouvelle donnée
                        //Données de la ligne
                        Ligne l = new Ligne(
                                -1,
                                cellules[posIdLigne],
                                cellules[posNomLigne],
                                cellules[posTypeLigne],
                                Integer.valueOf(cellules[posOrdreLigne]),
                                cellules[posCouleurLigne],
                                cellules[posVille],
                                idRegion);

                        //On sauvegarde
                        ligneCtrl.create(l);
                    } else if(since < vMaj && to >= vMaj)
                    {
                        //On va récupérer la ligne
                        Ligne l = ligneCtrl.get(cellules[posIdLigne]);

                        //Exception : GL
                        if(l == null && cellules[posIdLigne].equals("GL"))
                            l = ligneCtrl.get("");

                        //On la met à jour
                        l.setNom(cellules[posNomLigne]);
                        l.setType(cellules[posTypeLigne]);
                        l.setOrdre(Integer.valueOf(cellules[posOrdreLigne]));
                        l.setCouleur(cellules[posCouleurLigne]);
                        l.setVille(cellules[posVille]);
                        ligneCtrl.update(l);
                        updateNbGaresDansLigne(bdd, l);
                    } else if(since < vSupprime && to >= vSupprime)
                    {
                        //On va récupérer la gare
                        Ligne l = ligneCtrl.get(cellules[posIdLigne]);
                        //Et on supprime
                        if(l != null)
                            ligneCtrl.delete(l.getId());
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Lignes", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Lignes", "Problème d'entrée/sortie");
        }

        // TODO : Vérifier les succès si on a atteint des lignes ou pas (et inversement)
    }

    public static void updateDataGareDansLigne(Context contexte, SQLiteDatabase bdd, int since, int to, long idRegion, String ville)
    {
        String name = ville + "/" + "GaresDansLigne.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        GareCtrl gareCtrl = new GareCtrl(bdd);
        LigneCtrl ligneCtrl = new LigneCtrl(bdd);
        GareDansLigneCtrl gdlCtrl = new GareDansLigneCtrl(bdd);

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdGare = -1, posIdLigne = -1, posOrdre = -1, posPDLFond = -1, posPDLPoint = -1;
            int posNom = -1, posSurTitre = -1, posSousTitre = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("idLigne"))
                            posIdLigne = j;
                        else if(c.equalsIgnoreCase("idGare"))
                            posIdGare = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNom = j;
                        else if(c.equalsIgnoreCase("surTitre"))
                            posSurTitre = j;
                        else if(c.equalsIgnoreCase("sousTitre"))
                            posSousTitre = j;
                        else if(c.equalsIgnoreCase("ordre"))
                            posOrdre = j;
                        else if(c.equalsIgnoreCase("PDLFond"))
                            posPDLFond = j;
                        else if(c.equalsIgnoreCase("PDLPoint"))
                            posPDLPoint = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vCreation = Integer.valueOf(cellules[posVCreation]);
                    int vMaj = Integer.valueOf(cellules[posVMaj]);
                    int vSupprime = Integer.valueOf(cellules[posVSupprime]);

                    //Infos gare
                    if(since < vCreation && (to == -1 || to >= vCreation))
                    {
                        //Nouvelle donnée
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        Ligne l = ligneCtrl.get(cellules[posIdLigne], contexte);
                        int ordre = Integer.parseInt(cellules[posOrdre]);
                        int pdlFond = Integer.parseInt(cellules[posPDLFond]);
                        int pdlPoint = Integer.parseInt(cellules[posPDLPoint]);

                        String nom = null;
                        if(posNom != -1 && cellules[posNom] != "")
                        {
                            nom = cellules[posNom];
                        }

                        String surTitre = null;
                        if(posSurTitre != -1 && cellules[posSurTitre] != "")
                        {
                            surTitre = cellules[posSurTitre];
                        }

                        String sousTitre = null;
                        if(posSousTitre != -1 && cellules[posSousTitre] != "")
                        {
                            sousTitre = cellules[posSousTitre];
                        }

                        //On sauvegarde
                        if(g != null && l != null)
                        {
                            gdlCtrl.create(g, l, nom, surTitre, sousTitre, ordre, pdlFond, pdlPoint, idRegion);
                            updateNbGaresDansLigne(bdd, l);
                        }
                    } else if(since < vMaj && to >= vMaj)
                    {
                        //On va récupérer la ligne
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        Ligne l = ligneCtrl.get(cellules[posIdLigne]);
                        int ordre = Integer.parseInt(cellules[posOrdre]);
                        int pdlFond = Integer.parseInt(cellules[posPDLFond]);
                        int pdlPoint = Integer.parseInt(cellules[posPDLPoint]);
                        String nom = null;
                        if(posNom != -1 && cellules[posNom] != "")
                        {
                            nom = cellules[posNom];
                        }

                        String surTitre = null;
                        if(posSurTitre != -1 && cellules[posSurTitre] != "")
                        {
                            surTitre = cellules[posSurTitre];
                        }

                        String sousTitre = null;
                        if(posSousTitre != -1 && cellules[posSousTitre] != "")
                        {
                            sousTitre = cellules[posSousTitre];
                        }

                        if(g != null && l != null) {
                            gdlCtrl.update(g, l, nom, surTitre, sousTitre, ordre, pdlFond, pdlPoint);
                        }
                    } else if(since < vSupprime && to >= vSupprime)
                    {
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        Ligne l = ligneCtrl.get(cellules[posIdLigne]);

                        //Et on supprime
                        if(g != null && l != null) //Si c'est null alors la donnée est déjà supprimée
                            gdlCtrl.delete(g, l);

                        if(l != null)
                            updateNbGaresDansLigne(bdd, l);
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import GaresDansLigne", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import GaresDansLigne", "Problème d'entrée/sortie");
        }
    }

    public static void updateDataSucces(Context contexte, SQLiteDatabase bdd, int since, int to, long idRegion, String ville)
    {
        String fileName = "Succes.csv";
        try {
            String[] listeFichiers = contexte.getAssets().list(ville);
            if(listeFichiers == null)
                return;
            else {
                boolean hasFile = Arrays.asList(listeFichiers).contains(fileName);
                if(!hasFile) return;
            }
        } catch (IOException e) {
            Log.e("Import Succes", "Dossier de région non trouvé");
            return;
        }
        SuccesCtrl succesCtrl = new SuccesCtrl(bdd);
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        String name = ville + "/" + fileName;

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            // id;nom;type;conditions;vCreation;vMaj;vSuppression
            int posId = -1, posNom = -1, posType = -1, posConditions = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use semicolon as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("id"))
                            posId = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNom = j;
                        else if(c.equalsIgnoreCase("type"))
                            posType = j;
                        else if(c.equalsIgnoreCase("conditions"))
                            posConditions = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vCreation = Integer.valueOf(cellules[posVCreation]);
                    int vMaj = Integer.valueOf(cellules[posVMaj]);
                    int vSupprime = Integer.valueOf(cellules[posVSupprime]);

                    //Infos gare
                    if(since < vCreation && (to == -1 || to >= vCreation))
                    {
                        //Nouvelle donnée
                        String idExterne = cellules[posId];
                        String nom = cellules[posNom];
                        String condition = cellules[posConditions];
                        int type = Integer.parseInt(cellules[posType]);

                        //On sauvegarde
                        succesCtrl.create(idExterne, nom, type, condition, idRegion);
                    } else if(since < vMaj && to >= vMaj)
                    {
                        //On va récupérer la ligne
                        String idExterne = cellules[posId];
                        String nom = cellules[posNom];
                        String condition = cellules[posConditions];
                        int type = Integer.parseInt(cellules[posType]);

                        succesCtrl.update(idExterne, nom, type, condition, idRegion);
                    } else if(since < vSupprime && to >= vSupprime)
                    {
                        String idExterne = cellules[posId];

                        succesCtrl.delete(idExterne);
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Succes", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Succes", "Problème d'entrée/sortie");
        }

        // Forcer l’analyse des succès régionaux, pour savoir si on en a atteint ou pas (et inversement)
        ArrayList<Succes> succesAValider = succesCtrl.verifierSuccesRegionaux(idRegion);
        for(Succes succes: succesAValider)
        {
            succesCtrl.validerSucces(succes);
        }
    }

    public static void updateDataCoordonnees(Context contexte, SQLiteDatabase bdd, String ville, ArrayList<String> idGareAImporter)
    {
        String fileName = "Coordonnees.csv";
        try {
            String[] listeFichiers = contexte.getAssets().list(ville);
            if(listeFichiers == null)
                return;
            else {
                boolean hasFile = Arrays.asList(listeFichiers).contains(fileName);
                if(!hasFile) return;
            }
        } catch (IOException e) {
            Log.e("Import Coordonnees", "Dossier de région non trouvé");
            return;
        }
        CoordonneeCtrl coordonneesCtrl = new CoordonneeCtrl(bdd);
        GareCtrl gareCtrl = new GareCtrl(bdd);
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        String name = ville + "/" + fileName;

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            // idGare;latitude;longitude
            int posIdGare = -1, posLatitude = -1, posLongitude = -1;

            while ((line = br.readLine()) != null)
            {
                // use semicolon as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("idGare"))
                            posIdGare = j;
                        else if(c.equalsIgnoreCase("latitude"))
                            posLatitude = j;
                        else if(c.equalsIgnoreCase("longitude"))
                            posLongitude = j;
                        j++;
                    }
                } else {
                    //Infos gare
                    String idGare = cellules[posIdGare];

                    if(idGareAImporter == null || idGareAImporter.contains(idGare))
                    {
                        Gare g = gareCtrl.get(cellules[posIdGare]);
                        Double latitude = Double.parseDouble(cellules[posLatitude]);
                        Double longitude = Double.parseDouble(cellules[posLongitude]);

                        //On sauvegarde
                        coordonneesCtrl.create(g, latitude, longitude);
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Coordonnees", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Coordonnees", "Problème d'entrée/sortie");
        }
    }

    /**
     * Fonction permettant d'insérer une ligne si elle n'a pas été trouvée
     * @param contexte Contexte de l'activité
     * @param bdd Connexion à la base de données
     * @param idStif identifiant externe
     */
    public static Ligne insertDataUneLigne(Context contexte, SQLiteDatabase bdd, String idStif)
    {
        String ville = "Paris";
        int idRegion = 1; //Provisoire
        String name = ville + "/" + "Lignes.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        LigneCtrl ligneCtrl = new LigneCtrl(bdd);

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdLigne = -1, posNomLigne = -1, posTypeLigne = -1, posOrdreLigne = -1;
            int posCouleurLigne = -1, posVille = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("idExterne"))
                            posIdLigne = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomLigne = j;
                        else if(c.equalsIgnoreCase("type"))
                            posTypeLigne = j;
                        else if(c.equalsIgnoreCase("ordre"))
                            posOrdreLigne = j;
                        else if(c.equalsIgnoreCase("couleur"))
                            posCouleurLigne = j;
                        else if(c.equalsIgnoreCase("ville"))
                            posVille = j;
                        j++;
                    }
                } else {
                    String idLigneLigne = cellules[posIdLigne];

                    if(idStif.equals(idLigneLigne))
                    {
                        //Données de la ligne
                        Ligne l = new Ligne(
                                -1,
                                cellules[posIdLigne],
                                cellules[posNomLigne],
                                cellules[posTypeLigne],
                                Integer.valueOf(cellules[posOrdreLigne]),
                                cellules[posCouleurLigne],
                                cellules[posVille],
                                idRegion);

                        //On sauvegarde
                        return ligneCtrl.create(l);
                    }
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Lignes", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Lignes", "Problème d'entrée/sortie");
        }

        return null;
    }

    public static void reparerDonnees(Context contexte, SQLiteDatabase bdd)
    {
        // On commence par supprimer Null Island
        bdd.execSQL(GareBDD.TABLE_SUPPRIMER_GARE_NULLISLAND);

        emptyDataGareDansLigne(bdd);

        RegionCtrl regionCtrl = new RegionCtrl(bdd);
        for(Region r : regionCtrl.getAllRegions(RegionCtrl.REGIONSINSTALLE))
        {
            reparerGare(contexte, bdd, r.getDossierId());
            reparerLigne(contexte, bdd, r.getId(), r.getDossierId());
            updateDataGareDansLigne(contexte, bdd, 1, -1, r.getId(), r.getDossierId());
        }
    }

    private static void reparerGare(Context contexte, SQLiteDatabase bdd, String folderId)
    {
        String name = folderId + "/" + "Gares.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        GareCtrl gareCtrl = new GareCtrl(bdd);

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdGare = -1, posNomGare = -1, posExploitantGare = -1;
            int posLatitudeGare = -1, posLongitudeGare = -1;
            int posCouleur = -1, posCouleurEvo = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("id"))
                            posIdGare = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomGare = j;
                        else if(c.equalsIgnoreCase("exploitant"))
                            posExploitantGare = j;
                        else if(c.equalsIgnoreCase("latitude"))
                            posLatitudeGare = j;
                        else if(c.equalsIgnoreCase("longitude"))
                            posLongitudeGare = j;
                        else if(c.equalsIgnoreCase("couleur"))
                            posCouleur = j;
                        else if(c.equalsIgnoreCase("couleurEvolution"))
                            posCouleurEvo = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vSupprime = Integer.parseInt(cellules[posVSupprime]);

                    // On ne traite que les données non supprimés
                    if(vSupprime > 0) {
                        continue;
                    }

                    //On va récupérer la gare
                    Gare g = gareCtrl.get(cellules[posIdGare]);

                    // Si la gare existe déjà, on passe à la suivante
                    if(g != null)
                    {
                        continue;
                    }

                    //La gare n'a pas été trouvée, on la reconstruit
                    g = new Gare(-1, cellules[posIdGare], cellules[posNomGare],
                            Double.parseDouble(cellules[posLongitudeGare]),
                            Double.parseDouble(cellules[posLatitudeGare]),
                            cellules[posExploitantGare],
                            0,
                            Integer.parseInt(cellules[posCouleur]),
                            Integer.parseInt(cellules[posCouleurEvo]),
                            0,
                            null,
                            null);

                    //On sauvegarde
                    gareCtrl.create(g);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Reparer Gares", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Reparer Gares", "Problème d'entrée/sortie");
        }
    }

    private static void reparerLigne(Context contexte, SQLiteDatabase bdd, long idRegion, String folderId)
    {
        String name = folderId + "/" + "Lignes.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        LigneCtrl ligneCtrl = new LigneCtrl(bdd);

        try {
            br = new BufferedReader(new InputStreamReader(contexte.getAssets().open(name)));
            int i = 0;
            int posIdLigne = -1, posNomLigne = -1, posTypeLigne = -1, posOrdreLigne = -1;
            int posCouleurLigne = -1, posVille = -1;
            int posVCreation = -1, posVMaj = -1, posVSupprime = -1;

            while ((line = br.readLine()) != null)
            {
                // use comma as separator
                String[] cellules = line.split(cvsSplitBy);

                if(i == 0) //Première ligne
                {
                    int j = 0;
                    for(String c : cellules)
                    {
                        if(c.equalsIgnoreCase("idExterne"))
                            posIdLigne = j;
                        else if(c.equalsIgnoreCase("nom"))
                            posNomLigne = j;
                        else if(c.equalsIgnoreCase("type"))
                            posTypeLigne = j;
                        else if(c.equalsIgnoreCase("ordre"))
                            posOrdreLigne = j;
                        else if(c.equalsIgnoreCase("couleur"))
                            posCouleurLigne = j;
                        else if(c.equalsIgnoreCase("ville"))
                            posVille = j;
                        else if(c.equalsIgnoreCase("vCreation"))
                            posVCreation = j;
                        else if(c.equalsIgnoreCase("vMaj"))
                            posVMaj = j;
                        else if(c.equalsIgnoreCase("vSuppression"))
                            posVSupprime = j;
                        j++;
                    }
                } else {
                    //On regarde la date de création
                    int vSupprime = Integer.parseInt(cellules[posVSupprime]);

                    // On ne traite que les données non supprimés
                    if(vSupprime > 0) {
                        continue;
                    }

                    //On va récupérer la ligne
                    Ligne l = ligneCtrl.get(cellules[posIdLigne]);

                    // Si la ligne existe déjà, on passe à la suivante
                    if(l != null)
                    {
                        continue;
                    }

                    // La ligne n'a pas été trouvée, on la reconstruit
                    l = new Ligne(
                            -1,
                            cellules[posIdLigne],
                            cellules[posNomLigne],
                            cellules[posTypeLigne],
                            Integer.parseInt(cellules[posOrdreLigne]),
                            cellules[posCouleurLigne],
                            cellules[posVille],
                            idRegion);
                    ligneCtrl.update(l);
                    updateNbGaresDansLigne(bdd, l);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            Log.e("Import Lignes", "Fichier non trouvé");
        } catch (IOException e) {
            Log.e("Import Lignes", "Problème d'entrée/sortie");
        }

        // TODO : Vérifier les succès si on a atteint des lignes ou pas (et inversement)
    }
}
