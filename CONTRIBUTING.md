Comment contribuer ?
===================
Il y a plusieurs façons pour contribuer à ce projet :
 * En rapportant des bugs et des fautes d'orthographes
 * En soumettant des idées
 * En traduisant l'application dans d'autres langues
 * En proposant du code
 * En aidant à tester l'application

Système de ticket
------------------------
Le projet utilise un système de ticket, qu'il est possible de consulter : [Système de tickets](https://framagit.org/JonathanMM/passegares/issues). Vous pouvez en proposer de nouveaux, que ce soit des rapports de bugs ou des propositions de nouvelles fonctionnalités, discuter sur ceux déjà ouverts, et voter pour celles que vous attendez le plus. Les votes sont importants, car ils permettent de prioriser les développements.

Traduire l'application
----------------------------
Étant une application android, elle sait gérer plusieurs par défaut. Il suffit pour cela de traduire le fichier strings.xml présent sur le dépôt. Pleins de tutos sont disponible sur internet, ou il y a également l'excellente application [Stringlate](https://github.com/LonamiWebs/Stringlate) qui permet de traduire. S'il manque des chaînes de caractères présente dans l'application mais pas dans le fichier, n'hésitez pas à créer un ticket.

Tester l'application
-------------------------
Comme il existe de nombreux téléphones et tablettes où l'application peut s’exécuter, vous pouvez contribuer en testant l'application, et en faisant vos retours. Les plus développeurs d'entre vous peuvent également ajouter des tests unitaires, voir mettre en place des tests d'intégrations, qui seront ajoutés dans la chaîne d'intégration continue, et permettront de rendre l'application plus stable au fur et à mesure des nouvelles version. Si vous ne savez pas coder, écrire des scénarios de tests et/ou les exécuter manuellement est déjà d'une grande aide.

Corriger une faute
-----------------------
Vous constatez une faute dans un nom de gare ou de ligne ? [Suivez le guide pour corriger directement](https://framagit.org/JonathanMM/passegares/wikis/contribuer/faute-nom) !

Coder
-------
Si vous souhaitez coder, vous pouvez forker le dépôt, puis soumettre vos modifications via une requête de merge. Pour commencer à entrer dans le code, je vous conseille de regarder la liste des tickets et de résoudre des bugs simples (dans ce cas, pensez à vous assigner sur le ticket). Pour mettre en place l'environnement de développement, il vous faut android studio sur votre machine, ainsi qu'un terminal ou un émulateur.