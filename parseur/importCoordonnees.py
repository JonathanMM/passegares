import tools
import os
import csv

separator = ';'
print("Séparateur utilisé : " + separator)
dialect = csv.unix_dialect
dialect.delimiter = separator
encodingFile = 'utf-8-sig'
regionGare = 'Paris'
scriptFolder = os.path.dirname(os.path.realpath(__file__)) + "/"
regionPath = scriptFolder + '../app/src/main/assets/'+regionGare+'/'

# On commence par charger les gares
listeGaresRef = {}
with open(regionPath + "Gares.csv", "r", encoding='utf8') as fichiersGares:
    fData = csv.reader(fichiersGares, dialect)
    titre = next(fData)
    posCol = tools.getEntetesList(titre)
    for contenu in fData:
        listeGaresRef[contenu[posCol["idExterne"]]] = contenu[posCol["id"]]

with open("emplacement-des-gares-idf.csv", 'r', encoding=encodingFile) as fichierData:
    fData = csv.reader(fichierData, dialect)
    titre = next(fData)
    posCol = tools.getEntetesList(titre)

    listeGares = {}
    for contenu in fData:
        position = contenu[posCol['geo_point_2d']].split(",")
        latitude = position[0].strip()
        longitude = position[1].strip()
        idGare = contenu[posCol['id_ref_zdc']]
        idGareRef = contenu[posCol['id_ref_zda']]
        mode = contenu[posCol['mode']] 

        if idGareRef in listeGaresRef:
            idGare = listeGaresRef[idGareRef]
        else:
            print(">> Gare " + idGare + " non trouvé")

        if idGare in listeGares:
            listeGares[idGare].append([latitude, longitude, mode])
        else:
            listeGares[idGare] = [[latitude, longitude, mode]]
    
with open(regionPath + "Coordonnees.csv", "w", encoding='utf8') as fichierCoordonnees:
    fichierCoordonnees.write("idGare;latitude;longitude\n")
    for idGare in listeGares:
        coordonnees = listeGares[idGare]
        if len(coordonnees) > 1:
            print("Multiples coordonnées pour " + idGare)

            # On vérifie qu'on n'a pas que du train/RER
            diversifie = False
            for coordonnee in coordonnees:
                if coordonnee[2] != "RER" and coordonnee[2] != "TRAIN":
                    diversifie = True
                    break

            if diversifie:
                for coordonnee in coordonnees:
                    fichierCoordonnees.write(idGare + ";" + coordonnee[0]  + ";" + coordonnee[1] + "\n")
